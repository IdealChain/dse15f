#!/bin/bash -eu
DAYS="90"
HOST="localhost"

# cert files should stay private
umask 077

read -p "Common Name (CN) for cert [$HOST]: " -e -i "$HOST" HOST
SUBJ="/C=AT/ST=Vienna/L=Vienna/O=TU Wien/OU=Distributed Systems Engineering 15, Group 13/CN=$HOST"

echo Subject: $SUBJ
openssl req -new -x509 -nodes -out server.crt -keyout server.key -days $DAYS -subj "$SUBJ"
