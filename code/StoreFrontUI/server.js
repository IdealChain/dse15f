#!/usr/bin/env node
"use strict";

var express = require('express'),
    request = require('request'),
    log4js = require('log4js'),
    nconf = require('nconf'),
    fs = require('fs'),
    http = require('http'),
    https = require('https');

// Setup nconf to use in order:
// 1. Command-line arguments
// 2. Environment variables
// 3. A local config.json file
nconf.argv()
     .env()
     .file({ file: './config.json' });

var app = express();
app.use(log4js.connectLogger(log4js.getLogger('http'), {
    level: 'auto'
}));

// Forward API requests to API Gateway
var apiLogger = log4js.getLogger('api');
app.use('/api', function(req, res, next) {
    var url = nconf.get('api:url') + req.url;
    var stream = req.pipe(request(url));
    stream.on('error', function(err) {
        apiLogger.error("%s (%s)", err.message, url);
        res.status(500);
        next('The API gateway is dead, Jim.');
    });
    stream.pipe(res);
});

// Serve static assets folder
app.use(express.static('assets'));

// Check and read SSL cert
var sslCredentials = null;
if (nconf.get('https:keyFile') && nconf.get('https:certFile')) {
    try {
        var privateKey = fs.readFileSync(nconf.get('https:keyFile'), 'utf8');
        var certificate = fs.readFileSync(nconf.get('https:certFile'), 'utf8');
        sslCredentials = {key: privateKey, cert: certificate};
    } catch (e) {
        log4js.getLogger('https').warn("Key files not readable (%s)", e.code);
    }
}

// Start http/https servers
var httpServer = http.createServer(app);
httpServer.listen(nconf.get('http:port'), function() {
    var host = httpServer.address().address;
    var port = httpServer.address().port;
    console.log("WebUI serving on http://%s:%s. Come get yours!", host, port);
});

if (sslCredentials) {
    var httpsServer = https.createServer(sslCredentials, app);
    httpsServer.listen(nconf.get('https:port'), function() {
        var host = httpsServer.address().address;
        var port = httpsServer.address().port;
        console.log("Secure WebUI serving on https://%s:%s.", host, port);
    });
}
