"use strict";
(function () {
    var hour = 60 * 60 * 1000;
    var app = angular.module('wait4op', ['ngRoute']);

    app.config(function ($routeProvider, $locationProvider) {
        $locationProvider.hashPrefix('!');
        $routeProvider
            .when("/:view", {
                templateUrl: function (params) {
                    return "views/" + params.view + ".html";
                },
                controller: "SurgeryCtrl",
                controllerAs: "surgery"
            })
            .otherwise({redirectTo: "/public"});
    });

    app.factory('SurgeryService', function ($http, $filter, $rootScope, $interval, UserService) {
        var svc = {};
        svc.publicOPSlots = [];
        svc.publicOPSlotsStatus = {};
        svc.myOPSlots = [];
        svc.myOPSlotsStatus = {};

        function fetchSlots(url, target, status) {
            status.loading = true;

            $http.get(url).success(function (slots) {

                slots.map(function (slot) {
                    // enrich slot with formatted date and times for the UI
                    slot.date_ui = $filter('date')(slot.from);
                    slot.from_ui = $filter('date')(slot.from, 'shortTime');
                    slot.to_ui = $filter('date')(slot.to, 'shortTime');
                });

                angular.copy(slots, target);
                delete status.loading;
                delete status.error;

            }).error(function (data, statusCode) {
                status.error = statusCode;
                delete status.loading;
            });
        }

        svc.update = function () {
            fetchSlots('/api/publicOPSlots', svc.publicOPSlots, svc.publicOPSlotsStatus);

            if (UserService.isLoggedin())
                fetchSlots('/api/myOPSlots', svc.myOPSlots, svc.myOPSlotsStatus);
        };

        // clinic: delete a free slot
        svc.deleteFreeSlot = function (id) {
            $http.delete('/api/myOPSlots/' + id).success(function () {
                console.log("Slot deleted: " + id);
                svc.update();
            });
        };

        // doctor: delete a slot/patient reservation
        svc.deleteReservation = function (id) {
            $http.delete('/api/reservation/' + id).success(function () {
                console.log("Reservation deleted: " + id);
                svc.update();
            });
        };

        // init slotlist and update regularily / on user change
        svc.update();
        $interval(svc.update, 5000);
        $rootScope.$on('userChanged', svc.update);

        return svc;
    });

    app.factory('UserService', function ($http, $rootScope) {
        var svc = {};
        svc.roles = {
            PUBLIC: "public",
            PATIENT: "patient",
            DOCTOR: "doctor",
            CLINIC: "clinic"
        };

        svc.roleName = function (role) {
            switch (role ? role : svc.user.role) {
                case svc.roles.PATIENT:
                    return "Patient";
                case svc.roles.DOCTOR:
                    return "Arzt";
                case svc.roles.CLINIC:
                    return "Krankenhaus";
                default:
                    return "Öffentlichkeit";
            }
        };

        // reset all local user information to the default PUBLIC role
        function resetUser() {
            svc.user = {
                username: svc.roleName(svc.roles.PUBLIC),
                role: svc.roles.PUBLIC
            };
            $rootScope.$broadcast('userChanged');
        }

        function setUser(data) {
            if (!data)
                return false;

            // sanitize role
            if (data.role in svc.roles) {
                data.role = svc.roles[data.role];
            } else {
                console.log('Unknown role: ' + data.role);
                return false;
            }

            angular.copy(data, svc.user);
            $rootScope.$broadcast('userChanged');
            return true;
        }

        svc.login = function (username, password, success, error) {
            var credentials = {
                username: username,
                password: password
            };

            // login and retrieval of user object (fullname, role, etc)
            $http.post('/api/auth/login', credentials)
                .success(function (data, status) {
                    console.log('login success: ' + JSON.stringify(data));
                    if (setUser(data))
                        success();
                })
                .error(function (data, status) {
                    error(status);
                });
        };

        svc.logout = function () {
            $http.post('/api/auth/logout').success(function () {
                console.log('logout success.');
            });
            resetUser();
        };

        svc.isLoggedin = function () {
            return svc.user.role !== svc.roles.PUBLIC;
        };

        // initialize user and check whether there is already an API login session
        resetUser();
        $http.get('/api/auth/login').success(function (data, status) {
            setUser(data);
        });
        return svc;
    });

    app.controller('MainCtrl', function ($location, $scope, UserService) {
        this.userSvc = UserService;
        this.location = $location;

        this.loginDialog = {
            username: '',
            password: '',
            failure: '',
            busy: false
        };

        this.login = function (login) {
            login.failure = '';
            login.busy = true;

            UserService.login(login.username, login.password, function () {

                // success: clear & close dialog and switch to role specific page
                login.busy = false;
                login.username = login.password = '';
                $scope.loginForm.$setPristine();
                $scope.loginForm.$setUntouched();
                $('#loginModal').foundation('reveal', 'close');
                $location.path('/' + UserService.user.role);

            }, function (status) {
                // failure: show error
                login.failure = status;
                login.busy = false;
            });
        };

        this.logout = function () {
            UserService.logout();
            $location.path('/');
        };

    });

    app.controller('SurgeryCtrl', function ($routeParams, SurgeryService, UserService, ConfirmService) {
        this.userSvc = UserService;

        if ($routeParams.view === UserService.roles.PUBLIC) {
            this.ops = SurgeryService.publicOPSlots;
            this.status = SurgeryService.publicOPSlotsStatus;
        } else {
            this.ops = SurgeryService.myOPSlots;
            this.status = SurgeryService.myOPSlotsStatus;
        }

        this.search = '';
        this.order = 'from';

        this.setOrder = function (order) {
            // reverse order on second click
            if (this.order === order)
                order = '-' + order;

            this.order = order;
        };

        this.getSortClass = function (order, baseClass) {
            baseClass = baseClass || "fa-sort";
            if (this.order === order)
                return baseClass + "-asc";

            if (this.order === '-' + order)
                return baseClass + "-desc";

            return "fa-sort";
        };

        this.isFutureSlot = function (slot) {
            return slot.from >= Date.now();
        };

        // clinic: delete a free slot
        this.deleteFreeSlot = function (op) {

            var time = op.from_ui + ' — ' + op.to_ui;
            var text = 'Freien OP-Slot am ' + op.date_ui + ' um ' + time + ' wirklich löschen?';

            ConfirmService.confirm('Freien Slot löschen', text, function () {
                SurgeryService.deleteFreeSlot(op.id);
            });
        };

        // doctor: delete a slot/patient reservation
        this.deleteReservation = function (op) {

            var time = op.from_ui + ' — ' + op.to_ui;
            var text = 'Reservierung für PatientIn ' + op.patient + ' am ' + op.date_ui + ' um ' + time + ' wirklich stornieren?';

            ConfirmService.confirm('Reservierung stornieren', text, function () {
                SurgeryService.deleteReservation(op.id);
            });
        };
    });

    app.factory('ConfirmService', function ($rootScope) {
        var svc = {};

        svc.confirm = function (title, text, callback) {
            $rootScope.$broadcast('confirmPending', {title: title, text: text, ok: callback});
        };

        svc.subscribe = function (notify) {
            $rootScope.$on('confirmPending', function (event, args) {
                notify(args);
            });
        };

        return svc;
    });

    app.controller('ConfirmCtrl', function ($element, ConfirmService) {

        var self = this;
        self.confirmation = {title: 'Bestätigen', text: 'Wirklich?'};

        ConfirmService.subscribe(function (data) {
            angular.copy(data, self.confirmation);
            openReveal();
        });

        self.ok = function () {
            closeReveal();
            if (self.confirmation.ok) self.confirmation.ok();
        };

        self.cancel = closeReveal;

        function openReveal() {
            $element.foundation('reveal', 'open');
        }

        function closeReveal() {
            $element.foundation('reveal', 'close');
        }
    });

    // doctor: create a new patient/slot reservation request
    app.controller('CreateReservationCtrl', function ($element, $filter, $http) {
        var self = this;
        self.dialog = {
            patient: null,
            slotType: null,
            minSlotLength: 0,
            from: null,
            to: null,
            busy: false,
            failure: null
        };

        // init patient list
        self.patients = [];
        $http.get('/api/data/patients').success(function (data) {
            angular.copy(data, self.patients);
        });

        // init slot types list
        self.slotTypes = [];
        $http.get('/api/data/slotTypes').success(function (data) {
            angular.copy(data, self.slotTypes);
        });

        // minimal slot lengths to choose of
        self.slotLengths = [
            0,
            0.5 * hour,
            1 * hour,
            1.5 * hour,
            2 * hour,
            3 * hour,
            4 * hour
        ];

        // submit reservation request
        self.create = function () {
            self.dialog.busy = true;
            self.dialog.failure = null;

            var request = {
                patientId: self.dialog.patient.id,
                slotType: self.dialog.slotType,
                minSlotLength: self.dialog.minSlotLength,
                from: self.dialog.from,
                to: self.dialog.to
            };

            $http.post('/api/reservation', request)
                .success(function () {
                    self.dialog.busy = false;
                    $element.foundation('reveal', 'close');
                })
                .error(function (data, status) {
                    self.dialog.busy = false;
                    self.dialog.failure = status;
                });
        };
    });

    // clinic: create a new surgery slot
    app.controller('CreateSlotCtrl', function ($element, $filter, $http, SurgeryService) {
        var self = this;
        self.dialog = {
            slotType: null,
            from: null,
            to: null,
            busy: false,
            failure: null
        };

        // init slot types list
        self.slotTypes = [];
        $http.get('/api/data/slotTypes').success(function (data) {
            angular.copy(data, self.slotTypes);
        });

        // submit slot creation request
        self.create = function () {
            self.dialog.busy = true;
            self.dialog.failure = null;

            var request = {
                slotType: self.dialog.slotType,
                from: self.dialog.from,
                to: self.dialog.to
            };

            $http.post('/api/myOPSlots', request)
                .success(function () {
                    self.dialog.busy = false;
                    $element.foundation('reveal', 'close');
                    SurgeryService.update();
                })
                .error(function (data, status) {
                    self.dialog.busy = false;
                    self.dialog.failure = status;
                });
        };
    });

    app.directive('slotInput', function () {
        return {
            restrict: 'E',
            controller: 'SlotInputController',
            controllerAs: 'slot',
            bindToController: true,
            templateUrl: 'views/partials/slot-input.html',
            scope: {
                date: '=?',
                from: '=',
                to: '='
            }
        };
    });

    app.controller('SlotInputController', function () {
        var self = this;

        // JS date object of midnight
        self.date = null;

        // slot from/to milliseconds since 1970
        self.from = null;
        self.to = null;

        // available time options
        self.time_options = [];

        // date getter/setter function
        self.getset_date = function (date) {
            if (!arguments.length)
                return self.date;

            // date is undefined as long as the input is not valid
            self.date = date;
            if (date === undefined)
                return;

            // new date:
            date.setHours(0, 0, 0, 0);
            self.date = date;

            // update time selection options
            var time_options = [];
            for (var i = 0; i < 24; i += 0.5)
                time_options.push(self.date.getTime() + i * hour);
            angular.copy(time_options, self.time_options);

            // pre-set from/to selections
            self.from = date.getTime() + 6 * hour;
            self.to = date.getTime() + 20 * hour;
        };

        self.to_filter = function (value) {
            return value > self.from;
        };

        // init date: tomorrow
        self.getset_date(new Date(Date.now() + 24 * hour));
    });

    // foundation datepicker directive
    // http://stackoverflow.com/a/16727629
    app.directive('datepicker', function () {
        return {
            require: 'ngModel',
            link: function (scope, el, attr, ngModel) {
                $(el).fdatepicker({
                    format: 'yyyy-mm-dd',
                    language: 'de',
                    weekStart: 1,
                    onRender: function (date) {
                        return date.valueOf() < Date.now().valueOf() ? 'disabled' : '';
                    },
                    onSelect: function (dateText) {
                        scope.$apply(function () {
                            ngModel.$setViewValue(dateText);
                        });
                    }
                });
            }
        };
    });

    app.controller('NotificationsController', function (NotificationsService) {

        this.pending = NotificationsService.pending;
        this.dismiss = NotificationsService.dismiss;
        this.dismissAll = NotificationsService.dismissAll;

        this.hasError = function () {
            return NotificationsService.pending.some(function (n) {
                return n.isError;
            });
        };

        this.toolTip = function () {
            var pending = NotificationsService.pending.length;
            if (pending === 0)
                return 'Keine Nachrichten.';
            else if (pending === 1)
                return 'Eine Nachricht!';
            else
                return pending + ' Nachrichten!';
        };
    });

    app.factory('NotificationsService', function ($interval, $http, $rootScope, UserService) {
        var svc = {};

        // currently un-dismissed notifications
        svc.pending = [];
        // notification fetching timestamp: 'give me new notifications since fetched'
        svc.fetched = Date.now() - hour;

        svc.dismiss = function (notification) {
            var index = svc.pending.indexOf(notification);
            if (index > -1)
                svc.pending.splice(index, 1);
        };

        svc.dismissAll = function () {
            // clear array without creating a new instance
            svc.pending.length = 0;
        };

        svc.reset = function () {
            svc.fetched = Date.now() - hour;
            svc.dismissAll();
        };

        svc.update = function () {
            if (!UserService.isLoggedin())
                return;

            var url = '/api/notifications';

            if (svc.fetched !== undefined)
                url += '?since=' + svc.fetched;

            $http.get(url).success(function (result) {
                // append new notifications to the pending list
                svc.pending.push.apply(svc.pending, result.notifications);
                svc.fetched = result.timestamp;
            });
        };

        // update notification list regularily / on user change
        $interval(svc.update, 5000);
        $rootScope.$on('userChanged', function () {
            svc.reset();
            svc.update();
        });

        return svc;
    });

})();
