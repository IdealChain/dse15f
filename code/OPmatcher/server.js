#!/usr/bin/env node
"use strict";

var domain = require('domain');
var bunyan = require('bunyan');
var nconf = require('nconf');
var restify = require('restify');
var Request = require('./request');

// init configuration and logging
nconf.argv().env().file({file: './config.json'});
var log = bunyan.createLogger({name: 'OPmatcher'});

// init restify, log requests
var server = restify.createServer({log: log});
server.use(restify.bodyParser({ mapParams: false }));
server.use(restify.requestLogger());

// create json clients for used microservices
var services = {
    klinisys: restify.createJsonClient({
        url: nconf.get('services:klinisys'),
        name: 'klinisys'
    }),
    reservation: restify.createJsonClient({
        url: nconf.get('services:reservation'),
        name: 'reservation'
    }),
    notifier: restify.createJsonClient({
        url: nconf.get('services:notifier'),
        name: 'notifier'
    })
};

// default route: show bare routing endpoints
server.get('/', function(req, res, next) {
    var routes = Object.keys(server.router.mounts).map(function(key) {
        return server.router.mounts[key].spec;
    });

    res.send(routes);
    next();
});

// handle incoming reservation request
server.post('/requestReservation', function(req, res, next) {

    var request = new Request(req.log, services, req.body);

    // node.js domain to isolate errors occuring during processing
    domain.create()
        .on('error', function(err) {
            req.log.error(err, 'Reservation processing error');
        })
        .run(request.startProcessing.bind(request));

    res.send(200, { ok: true });
    next();
});

server.listen(nconf.get('http:port'), function () {
    log.info('OPmatcher ready at %s.', server.url);
});
