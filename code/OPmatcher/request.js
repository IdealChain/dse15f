"use strict";

var Q = require('q');
var util = require('util');
var chai = require('chai'), expect = chai.expect;
chai.use(require('chai2-json-schema'));

var requestSchema = {
    title: 'request',
    type: 'object',
    required: ['patientId', 'doctorId', 'slotType', 'from', 'to'],
    properties: {
        patientId: {
            type: 'string',
            minLength: 1
        },
        doctorId: {
            type: 'string',
            minLength: 1
        },
        slotType: {
            type: 'string',
            minLength: 1
        },
        minSlotLength: {
            type: 'number',
            minimum: 0
        },
        from: {
            type: 'number',
            minimum: 0
        },
        to: {
            type: 'number',
            minimum: 0
        }
    }
};

function Request(logger, services, data) {
    this.log = logger;
    this.services = services;
    this.data = data;

    // validate data format of incoming objects
    expect(this.services, 'services').to.be.jsonSchema({
        type: 'object',
        required: ['klinisys', 'reservation', 'notifier']
    });
    expect(this.data, 'request').to.be.jsonSchema(requestSchema);
    expect(this.data.to, 'to greater than from').to.be.above(this.data.from);
}

Request.prototype.getUser = function (userId) {
    return Q.ninvoke(this.services.klinisys, 'get', '/masterdata/' + userId).get(2)
        .then(function(user) {

            // only return non-sensitive data
            return {
                id: user.id,
                title: user.title,
                firstName: user.firstName,
                lastName: user.lastName
            };
        });
};

Request.prototype.getNearestClinics = function (patientId) {
    return Q.ninvoke(this.services.klinisys, 'get', '/masterdata/nearest/' + patientId).get(2)
        .then(function(clinics) {

            //only return non-sensitive data
            return clinics.map(function(clinic) {
                return {
                    id: clinic.id,
                    name: clinic.name
                };
            });
        });
};

Request.prototype.getFreeSlots = function () {
    var klinisys = this.services.klinisys;
    var reservation = this.services.reservation;

    return Q.ninvoke(klinisys, 'get', '/slots').get(2)
        .then(function (slots) {

            var slotIds = slots.map(function (slot) {
                return slot.id;
            });
            return Q.ninvoke(reservation, 'get', '/reservation?ids=' + slotIds.join("&ids=")).get(2)
                .then(function (reservations) {

                    var reservedSlotIds = reservations.map(function (reservedSlot) {
                        return reservedSlot.slotId;
                    });
                    var freeSlots = slots.filter(function (slot) {
                        return reservedSlotIds.indexOf(slot.id) === -1;
                    });

                    return freeSlots;
                });
        });
};

Request.prototype.startProcessing = function () {
    var self = this;
    self.log.info('Processing request (%j)', self.data);

    // fetch all required data from neighbor services
    Q.all([
        self.getNearestClinics(self.data.patientId),
        self.getUser(self.data.patientId),
        self.getUser(self.data.doctorId),
        self.getFreeSlots()

    ]).spread(function (nearestClinics, patient, doctor, freeSlots) {

        // validate received objects a bit
        expect(patient.id).to.equal(self.data.patientId);
        expect(doctor.id).to.equal(self.data.doctorId);
        expect(nearestClinics).to.be.an('array');
        expect(freeSlots).to.be.an('array');

        self.data.patient = patient;
        self.data.doctor = doctor;

        self.log.info('Collected %d clinics, %d free slots.', nearestClinics.length, freeSlots.length);
        self.assignSlot(nearestClinics, freeSlots);

    }).done();
};

Request.prototype.assignSlot = function (nearestClinics, freeSlots) {
    var self = this;
    self.log.info('Finding a slot');

    // group free slots by clinic
    var clinicSlots = freeSlots.reduce(function (slots, slot) {

        var clinicId = slot.hospitalId;
        if (slots[clinicId] === undefined)
            slots[clinicId] = [];

        slots[clinicId].push(slot);
        return slots;
    }, {});

    // suitable slot: within specified timespan and with needed type
    var suitableSlotsFilter = function (slot) {
        var length = (slot.to - slot.from);
        return self.data.from <= slot.from && self.data.to >= slot.to &&
            self.data.slotType === slot.type && length >= self.data.minSlotLength;
    };

    var assignedClinic;
    var assignedSlot;

    // assignment: walk through clinics, find first free, suitable slot
    var clinicsLength = nearestClinics.length;
    for (var c = 0; c < clinicsLength; c++) {
        var clinic = nearestClinics[c];

        // check if clinic has no free slots
        if (clinicSlots[clinic.id] === undefined)
            continue;

        // filter out suitable slots within timespan and with desired type
        var suitableSlots = clinicSlots[clinic.id].filter(suitableSlotsFilter);
        if (!suitableSlots.length)
            continue;

        // take first suitable slot
        assignedSlot = suitableSlots.shift();
        assignedClinic = clinic;
        break;
    }

    if (assignedSlot !== undefined)
        self.postReservation(assignedSlot, assignedClinic);
    else
        self.noSlotFound();
};

Request.prototype.postReservation = function (slot, clinic) {
    var self = this;

    var reservation = {
        slotId: slot.id,
        patientId: self.data.patientId,
        doctorId: self.data.doctorId
    };

    self.log.info('Posting reservation: %j', reservation);
    self.services.reservation.post('/reservation', reservation, function (err) {
        if (err) throw err;
        self.slotReserved(slot, clinic);
    });
};

Request.prototype.noSlotFound = function () {
    this.log.warn('No suitable slot found.');

    // notify requesting doctor about failed slot assignment
    var doctorNotification = {
        userId: this.data.doctorId,
        message: 'Kein passender OP-Slot gefunden!',
        isError: true,
        context: {
            patient: this.data.patient
        }
    };

    this.services.notifier.post('/notifications', doctorNotification, function (err) {
        if (err) throw err;
    });
};

Request.prototype.slotReserved = function (slot, clinic) {
    this.log.info('Slot assigned: %j', slot);

    // notify affected doctor, clinic and patient
    var message = "OP-Slot zugewiesen!";

    var doctorNotification = {
        userId: this.data.doctor.id,
        message: message,
        context: {
            patient: this.data.patient,
            clinic: clinic,
            slot: slot
        }
    };

    var clinicNotification = {
        userId: clinic.id,
        message: message,
        context: {
            patient: this.data.patient,
            doctor: this.data.doctor,
            slot: slot
        }
    };

    var patientNotification = {
        userId: this.data.patient.id,
        message: message,
        context: {
            doctor: this.data.doctor,
            clinic: clinic,
            slot: slot
        }
    };

    function notificationPostCallback(err) {
        if (err) throw err;
    }

    this.services.notifier.post('/notifications', doctorNotification, notificationPostCallback);
    this.services.notifier.post('/notifications', clinicNotification, notificationPostCallback);
    this.services.notifier.post('/notifications', patientNotification, notificationPostCallback);
};

module.exports = Request;
