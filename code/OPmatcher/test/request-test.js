"use strict";

var Q = require('q');
var expect = require('chai').expect;
var Request = require('../request');

function getLoggerTemplate() {
    return {
        info: function() {},
        error: function() {}
    };
}

function getServicesTemplate() {
    return {
        klinisys: null,
        reservation: null,
        notifier: null
    };
}

function getRequestDataTemplate() {
    return {
        patientId: 'abc',
        doctorId: 'xyz',
        slotType: 'Augen',
        from: 0,
        to: Date.now()
    };
}

describe('request', function () {
    describe('Request()', function() {

        it('should construct', function() {
            var r = new Request(getLoggerTemplate(), getServicesTemplate(), getRequestDataTemplate());
        });

        it('should throw on invalid schema', function() {
            function initEmpty() {
                var r = new Request(getLoggerTemplate(), getServicesTemplate(), {});
            }
            expect(initEmpty).to.throw('Missing required property');
        });

    });

    describe('getUser()', function() {
        it('should return user', function(ok) {
            var patient = {
                id: 'abc',
                title: '',
                firstName: 'Hugo',
                lastName: 'Tester'
            };

            var services = getServicesTemplate();
            services.klinisys = {
                get: function(url, callback) {
                    expect(url).to.equal('/masterdata/abc');
                    callback(null, null, null, patient);
                }
            };

            var r = new Request(getLoggerTemplate(), services, getRequestDataTemplate());
            r.getUser(patient.id)
                .then(function(result) {
                    expect(result).to.deep.equal(patient);
                    ok();
                })
                .done();
        });
    });

    describe('getNearestClinics()', function() {
        it('should return clinics', function(ok) {
            var clinics = [
                { id: 'smz', name: 'SMZ Ost'},
                { id: 'ukh', name: 'UKH' }
            ];

            var services = getServicesTemplate();
            services.klinisys = {
                get: function(url, callback) {
                    expect(url).to.equal('/masterdata/nearest/abc');
                    callback(null, null, null, clinics);
                }
            };

            var r = new Request(getLoggerTemplate(), services, getRequestDataTemplate());
            r.getNearestClinics('abc')
                .then(function(result) {
                    expect(result).to.deep.equal(clinics);
                    ok();
                })
                .done();
        });
    });

    describe('getFreeSlots()', function() {
        it('should return only free slots', function(ok) {
            var slots = [
                { id: 'abc' },
                { id: 'xyz' }
            ];
            var reservations = [
                { slotId: 'xyz' }
            ];

            var services = getServicesTemplate();
            services.klinisys = {
                get: function(url, callback) {
                    expect(url).to.equal('/slots');
                    callback(null, null, null, slots);
                }
            };
            services.reservation = {
                get: function(url, callback) {
                    expect(url).to.equal('/reservation?ids=abc&ids=xyz');
                    callback(null, null, null, reservations);
                }
            };

            var r = new Request(null, services, getRequestDataTemplate());
            r.getFreeSlots()
                .then(function(result) {
                    expect(result).to.deep.equal([
                        { id: 'abc' }
                    ]);
                    ok();
                })
                .done();
        });
    });

    describe('assignSlot()', function() {
        var nearestClinics = [
            { id: 'smz', name: 'SMZ Ost'},
            { id: 'ukh', name: 'UKH' }
        ];
        var freeSlots = [
            { id: 'abc', hospitalId: 'smz', from: 60000, to: 120000, type: 'Augen' }
        ];

        var getRequestDataTemplate = function() {
            return {
                patientId: 'abc',
                doctorId: 'xyz',
                slotType: 'Augen',
                from: 0,
                to: 300000,
                minSlotLength: 60000
            };
        };

        it('should assign free slot', function(ok) {
            var services = getServicesTemplate();
            services.reservation = {
                post: function(url, reservation, callback) {
                    expect(url).to.equal('/reservation');
                    expect(reservation).to.deep.equal({
                        slotId: 'abc',
                        patientId: 'abc',
                        doctorId: 'xyz'
                    });
                    callback(null);
                }
            };

            var r = new Request(getLoggerTemplate(), services, getRequestDataTemplate());
            r.slotReserved = function(slot, clinic) {
                ok();
            };
            r.assignSlot(nearestClinics, freeSlots);
        });

        it('should not assign different slotType', function(ok) {
            var data = getRequestDataTemplate();
            data.slotType = 'Kardiologie';

            var services = getServicesTemplate();
            services.reservation = {
                post: function(url, reservation, callback) {
                    ok(false);
                }
            };

            var r = new Request(getLoggerTemplate(), services, data);
            r.noSlotFound = ok;
            r.assignSlot(nearestClinics, freeSlots);
        });

        it('should not assign too long minSlotLength', function(ok) {
            var data = getRequestDataTemplate();
            data.minSlotLength = 70000;

            var services = getServicesTemplate();
            services.reservation = {
                post: function(url, reservation, callback) {
                    ok(false);
                }
            };

            var r = new Request(getLoggerTemplate(), services, data);
            r.noSlotFound = ok;
            r.assignSlot(nearestClinics, freeSlots);
        });

        it('should not assign different slot from/to', function(ok) {
            var data = getRequestDataTemplate();
            data.from = 100000;
            data.to = 160000;

            var services = getServicesTemplate();
            services.reservation = {
                post: function(url, reservation, callback) {
                    ok(false);
                }
            };

            var r = new Request(getLoggerTemplate(), services, data);
            r.noSlotFound = ok;
            r.assignSlot(nearestClinics, freeSlots);
        });
    });

});
