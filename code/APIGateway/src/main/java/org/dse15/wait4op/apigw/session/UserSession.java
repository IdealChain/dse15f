package org.dse15.wait4op.apigw.session;

import org.dse15.wait4op.apigw.model.User;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Handles the Session-User association
 */
public class UserSession {

    private Map<String, User> _activeSessions = new HashMap<>();

    public boolean isLoggedIn(String sessionId) {
        return _activeSessions.containsKey(sessionId);
    }

    /**
     *
     * @param sessionId
     * @return
     * @throws SessionNotFoundException
     */
    public User getUser(String sessionId) throws SessionNotFoundException {
        if (isLoggedIn(sessionId))
            return _activeSessions.get(sessionId);

        throw new SessionNotFoundException("Invalid or expired session");
    }

    /**
     * Associates the User object with the given sessionId.
     * Existing Sessions will be overwritten
     *
     * @param user
     * @exception NullPointerException
     */
    public String login(User user) {
        if (user == null) {
            throw new NullPointerException("User must both be not-null.");
        }

        String sessionId = generateSessionId();
        _activeSessions.put(sessionId, user);
        return sessionId;
    }

    /**
     * Removes the session
     * @param sessionId
     */
    public void logout(String sessionId) {
        _activeSessions.remove(sessionId);
    }

    private String generateSessionId() {
        return UUID.randomUUID().toString();
    }
}
