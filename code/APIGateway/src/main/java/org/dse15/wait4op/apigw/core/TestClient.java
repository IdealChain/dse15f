package org.dse15.wait4op.apigw.core;

import org.dse15.wait4op.apigw.core.exception.RemoteMicroServiceException;
import org.dse15.wait4op.apigw.core.interfaces.IClient;

import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringJoiner;

/**
 *
 */
public class TestClient implements IClient {

    //
    // URLS
    //
    private static final String KLINISYS_LOGIN = "^http://localhost:8082/masterdata/login\\?username=.*";
    private static final String KLINISYS_ALLUSERS = "^http://localhost:8082/masterdata$";
    private static final String KLINISYS_SLOT = "^http://localhost:8082/slots/\\?.+";
    private static final String KLINISYS_ALLSLOTS = "^http://localhost:8082/slots$";
    private static final String RESER_ALL = "^http://localhost:8084/reservation\\?ids=.*";

    //
    // Data
    //
    private static final Map<String, String> USERS = new HashMap<>();       //username, json-string from KLINIsys
    private static final String NO_USER = "{ \"error\": \"NOT_FOUND\"}";
    private static final Map<String, String> SLOTS = new HashMap<>();       //id, json-string of slot
    private static final Map<String, String> RESERVATIONS = new HashMap<>();        //slotid, json-string of reservation

    static {
        USERS.put("adelheid", "{\"title\":\"Fr.\",\"firstName\":\"Adelheid\",\"lastName\":\"Abesser\",\"id\":\"64c582ef\",\"type\":\"PATIENT\",\"loginName\":\"adelheid\"}");
        USERS.put("albert", "{\"title\":\"Dr.\",\"firstName\":\"Albert\",\"lastName\":\"Aufschneider\",\"id\":\"44249278\",\"type\":\"DOCTOR\",\"loginName\":\"albert\"}");
        USERS.put("smzost", "{\"title\":\"\",\"firstName\":\"\",\"lastName\":\"\",\"id\":\"6c17a3ad\",\"type\":\"HOSPITAL\",\"loginName\":\"smzost\", \"name\": \"SMZ Ost\"}");
        USERS.put("lkhdo", "{\"title\":\"\",\"firstName\":\"\",\"lastName\":\"\",\"id\":\"5b06a2ac\",\"type\":\"HOSPITAL\",\"loginName\":\"lkhdo\", \"name\": \"LKH Dornbirn\"}");

        SLOTS.put("bb56959f",
                "{\"id\":\"bb56959f\", \"type\":\"Augenheilkunde\", \"from\":\"1432646765000\", \"to\":\"1432650365000\", \"hospitalId\": \"6c17a3ad\" }");
        SLOTS.put("e9725d82",
                "{\"id\":\"e9725d82\", \"type\":\"Augenheilkunde\", \"from\":\"1432648765000\", \"to\":\"1432652365000\", \"hospitalId\": \"6c17a3ad\" }");
        SLOTS.put("fe15d84b",
                "{\"id\":\"fe15d84b\", \"type\":\"HNO\", \"from\":\"1432649765000\", \"to\":\"1432653365000\", \"hospitalId\": \"5b06a2ac\" }");
        SLOTS.put("6b7e266b",
                "{\"id\":\"6b7e266b\", \"type\":\"HNO\", \"from\":\"1432650765000\", \"to\":\"1432654365000\", \"hospitalId\": \"5b06a2ac\" }");

        RESERVATIONS.put("bb56959f",
                "{\"slotId\": \"bb56959f\", \"doctorId\": \"44249278\", \"patientId\": \"64c582ef\"}");
    }

    @Override
    public String getResponse(String url, String content, String method) throws RemoteMicroServiceException {

        if (url == null || method == null)
            throw new RemoteMicroServiceException("URL and/or METHOD must not be null");

        Map<String, String> query = splitQuery(url);

        //
        // User by username/password
        if (url.matches(KLINISYS_LOGIN) && method.equalsIgnoreCase("get") && query.containsKey("username")) {
            if (query.get("username").equals(query.get("password")) && USERS.containsKey(query.get("username"))) {
                return USERS.get(query.get("username"));
            } else {
                return NO_USER;
            }
        }
        //
        // All users
        else if (url.matches(KLINISYS_ALLUSERS) && method.equalsIgnoreCase("get")) {
            StringJoiner sj = new StringJoiner(",");
            USERS.keySet().forEach(key -> sj.add(USERS.get(key)));
            return "[" + sj.toString() + "]";
        }
        //
        // Slot id
        else if (url.matches(KLINISYS_SLOT) && method.equalsIgnoreCase("get")) {

        }
        //
        // All slots
        else if (url.matches(KLINISYS_ALLSLOTS) && method.equalsIgnoreCase("get")) {
            StringJoiner sj = new StringJoiner(",");
            SLOTS.keySet().forEach(key -> sj.add(SLOTS.get(key)));
            return "[" + sj.toString() + "]";
        }
        //
        // reservations
        else if (url.matches(RESER_ALL) && method.equalsIgnoreCase("get")) {
            StringJoiner sj = new StringJoiner(",");
            RESERVATIONS.keySet().forEach(key -> sj.add(RESERVATIONS.get(key)));
            return "[" + sj.toString() + "]";
        }

        throw new RemoteMicroServiceException("URL/METHOD '" + url + "'/" + method + " not found");
    }

    //Credits to http://stackoverflow.com/a/13592567
    private static Map<String, String> splitQuery(String urlStr) {
        try {
            URL url = new URL(urlStr);
            Map<String, String> query_pairs = new LinkedHashMap<>();
            String query = url.getQuery();
            String[] pairs = query.split("&");
            for (String pair : pairs) {
                int idx = pair.indexOf("=");
                query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
            }
            return query_pairs;
        } catch (Exception e) {
            return new LinkedHashMap<>();
        }
    }
}
