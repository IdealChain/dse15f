package org.dse15.wait4op.apigw.model.frontend;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 */
public class SlotTemplate {

    private String _type;
    private long _from;
    private long _to;

    @JsonProperty("slotType")
    public String getType() {
        return _type;
    }

    public void setType(String type) {
        _type = type;
    }

    @JsonProperty("from")
    public long getFrom() {
        return _from;
    }

    public void setFrom(long from) {
        _from = from;
    }

    @JsonProperty("to")
    public long getTo() {
        return _to;
    }

    public void setTo(long to) {
        _to = to;
    }
}
