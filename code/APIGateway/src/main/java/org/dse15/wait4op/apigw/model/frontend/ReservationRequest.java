package org.dse15.wait4op.apigw.model.frontend;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 */
public class ReservationRequest {
    private String _patientId;
    private String _doctorId;
    private String _slotType;
    private long _minSlotLength;
    private long _from;
    private long _to;

    @JsonProperty("patientId")
    public String getPatientId() {
        return _patientId;
    }

    public void setPatientId(String patientId) {
        _patientId = patientId;
    }

    @JsonProperty(value = "doctorId", defaultValue = "")
    public String getDoctorId() {
        return _doctorId;
    }

    public void setDoctorId(String doctorId) {
        _doctorId = doctorId;
    }

    @JsonProperty("slotType")
    public String getSlotType() {
        return _slotType;
    }

    public void setSlotType(String type) {
        _slotType = type;
    }

    @JsonProperty(value = "minSlotLength", defaultValue = "0")
    public long getMinSlotLength() {
        return _minSlotLength;
    }

    public void setMinSlotLength(long length) {
        _minSlotLength = length;
    }

    @JsonProperty("from")
    public long getFrom() {
        return _from;
    }

    public void setFrom(long from) {
        _from = from;
    }

    @JsonProperty("to")
    public long getTo() {
        return _to;
    }

    public void setTo(long to) {
        _to = to;
    }
}
