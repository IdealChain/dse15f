package org.dse15.wait4op.apigw.model.frontend;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 */
public class Credentials {

    private String _username;
    private String _password;

    @JsonProperty
    public String getUsername() {
        return _username;
    }

    public void setUsername(String username) {
        _username = username;
    }

    @JsonProperty
    public String getPassword() {
        return _password;
    }

    public void setPassword(String password) {
        _password = password;
    }
}