package org.dse15.wait4op.apigw.core.interfaces;

import org.dse15.wait4op.apigw.core.exception.APIGatewayException;
import org.dse15.wait4op.apigw.core.exception.RemoteMicroServiceException;
import org.dse15.wait4op.apigw.model.frontend.ReservationRequest;
import org.dse15.wait4op.apigw.model.frontend.Slot;
import org.dse15.wait4op.apigw.model.User;
import org.dse15.wait4op.apigw.session.SessionException;

import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 *
 */
public interface IAPIFacade {

    User getUser(String username, String password) throws RemoteMicroServiceException, APIGatewayException;

    User getUser(String userId) throws RemoteMicroServiceException, APIGatewayException;

    List<User> getPatients(User user) throws RemoteMicroServiceException, APIGatewayException, SessionException;

    List<Slot> getAllSurgerySlots(User user) throws RemoteMicroServiceException, APIGatewayException;

    Slot getSurgerySlot(User user, String slotId) throws RemoteMicroServiceException, APIGatewayException;

    Slot createSurgerySlot(User user, long from, long to, String type) throws RemoteMicroServiceException, APIGatewayException, SessionException;

    void deleteSurgerySlot(User user, String slotId) throws RemoteMicroServiceException, APIGatewayException, SessionException;

    void requestReservation(User user, ReservationRequest reservationRequest) throws RemoteMicroServiceException, APIGatewayException, SessionException;

    void deteteReservation(User user, String slotId) throws RemoteMicroServiceException, APIGatewayException, SessionException;

    List<String> getSlotTypes(User user) throws RemoteMicroServiceException, APIGatewayException, SessionException;

    String getNotifications(User user, String since) throws RemoteMicroServiceException, APIGatewayException, SessionException;

    void registerMicroservices(Map<String, URL> microservices);

    void registerClient(IClient client);
}
