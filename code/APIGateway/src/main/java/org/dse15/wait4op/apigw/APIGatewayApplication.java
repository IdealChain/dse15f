package org.dse15.wait4op.apigw;

import com.codahale.metrics.servlets.PingServlet;
import com.netflix.hystrix.contrib.metrics.eventstream.HystrixMetricsStreamServlet;
import io.dropwizard.Application;
import io.dropwizard.client.HttpClientBuilder;
import io.dropwizard.setup.Environment;
import org.apache.http.client.HttpClient;
import org.dse15.wait4op.apigw.core.CoreFactory;
import org.dse15.wait4op.apigw.core.interfaces.IAPIFacade;
import org.dse15.wait4op.apigw.health.RemoteClientHealthCheck;
import org.dse15.wait4op.apigw.resources.*;
import org.dse15.wait4op.apigw.session.UserSession;
import org.dse15.wait4op.apigw.util.Constants;
import org.eclipse.jetty.servlet.ServletHolder;

/**
 *
 */
public class APIGatewayApplication extends Application<APIGatewayConfiguration> {

    public static void main(String[] args) throws Exception {
        new APIGatewayApplication().run(args);
    }

    @Override
    public String getName() {
        return Constants.APP_NAME;
    }

    @Override
    public void run(APIGatewayConfiguration config, Environment environment) throws Exception {
        //
        // init internal components
        IAPIFacade facade = CoreFactory.createAPIFacade();
        facade.registerMicroservices(config.getMicroservices());
        HttpClient hc = new HttpClientBuilder(environment).using(config.getHttpClientConfiguration()).build("Client");
        facade.registerClient(CoreFactory.createRemoteClient(hc));
        UserSession us = new UserSession();

        //
        // init API endpoints
        environment.jersey().register(new AuthResource(facade, us));
        environment.jersey().register(new MasterdataResource(facade, us));
        environment.jersey().register(new SurgeryResource(facade, us));
        environment.jersey().register(new ReservationResource(facade, us));
        environment.jersey().register(new NotificationResource(facade, us));
        environment.jersey().register(new HelpResource() {{
            init(environment.jersey().getResourceConfig().getEndpointsInfo());
        }});

        //
        // init debug/monitor endpoints
        environment.getApplicationContext().addServlet(new ServletHolder(new PingServlet()), "/ping");
        environment.getApplicationContext().addServlet(new ServletHolder(new HystrixMetricsStreamServlet()), "/hystrix.stream");

        environment.healthChecks().register("remoteClient", new RemoteClientHealthCheck(hc));
    }
}