package org.dse15.wait4op.apigw;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.client.HttpClientConfiguration;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URL;
import java.util.Collections;
import java.util.Map;

/**
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class APIGatewayConfiguration extends Configuration {

    @NotNull
    private Map<String, URL> _microservices = Collections.emptyMap();

    @Valid @NotNull
    private HttpClientConfiguration _httpClientConfiguration = new HttpClientConfiguration();

    private int _microserviceTimeout;

    @JsonProperty("microservices")
    public Map<String, URL> getMicroservices() {
        return _microservices;
    }

    @JsonProperty("microservices")
    public void setMicroservices(Map<String, URL> microservices) {
        _microservices = microservices;
    }

    @JsonProperty("httpClient")
    public HttpClientConfiguration getHttpClientConfiguration() {
        return _httpClientConfiguration;
    }

    @JsonProperty("httpClient")
    public void setHttpClientConfiguration(HttpClientConfiguration hcc) {
        _httpClientConfiguration = hcc;
    }

    @JsonProperty("microserviceTimeout")
    public int getMicroserviceTimeout() {
        return _microserviceTimeout;
    }

    @JsonProperty("microserviceTimeout")
    public void setMicroserviceTimeout(int microserviceTimeout) {
        _microserviceTimeout = microserviceTimeout;
    }
}