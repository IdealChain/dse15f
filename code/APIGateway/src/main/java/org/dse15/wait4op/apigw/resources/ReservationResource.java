package org.dse15.wait4op.apigw.resources;

import io.dropwizard.jersey.params.IntParam;
import io.dropwizard.jersey.sessions.Session;
import org.dse15.wait4op.apigw.core.interfaces.IAPIFacade;
import org.dse15.wait4op.apigw.model.frontend.ReservationRequest;
import org.dse15.wait4op.apigw.session.SessionException;
import org.dse15.wait4op.apigw.session.SessionNotFoundException;
import org.dse15.wait4op.apigw.session.UserSession;
import org.dse15.wait4op.apigw.util.Constants;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;

/**
 *
 */
@Path("/reservation")
@Produces(MediaType.APPLICATION_JSON)
public class ReservationResource extends BaseResource {

    public ReservationResource(IAPIFacade dataAggregator, UserSession userSession) {
        super(dataAggregator, userSession);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createReservationRequest(ReservationRequest reservationRequest, @CookieParam(Constants.AUTH_COOKIE_KEY) String authCookie) {
        try {

            getAPIFacade().requestReservation(getUserSession().getUser(authCookie), reservationRequest);
            return Response.ok().build();

        } catch (SessionException e) {
            return toResponse(new WebApplicationException(e.getMessage(), e, Response.Status.FORBIDDEN));
        } catch (Exception e) {
            e.printStackTrace();
            return toResponse(new WebApplicationException(e.getMessage(), e, Response.Status.INTERNAL_SERVER_ERROR));
        }
    }

    @DELETE
    @Path("/{slotId}")
    public Response deleteReservation(@PathParam("slotId")String slotId,
                                      @CookieParam(Constants.AUTH_COOKIE_KEY) String authCookie) {
        try {

            getAPIFacade().deteteReservation(getUserSession().getUser(authCookie), slotId);
            return Response.ok().build();

        } catch (SessionException e) {
            return toResponse(new WebApplicationException(e.getMessage(), e, Response.Status.FORBIDDEN));
        } catch (Exception e) {
            e.printStackTrace();
            return toResponse(new WebApplicationException(e.getMessage(), e, Response.Status.INTERNAL_SERVER_ERROR));
        }
    }
}
