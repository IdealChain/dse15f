package org.dse15.wait4op.apigw.resources;

import org.dse15.wait4op.apigw.core.interfaces.IAPIFacade;
import org.dse15.wait4op.apigw.model.frontend.Slot;
import org.dse15.wait4op.apigw.model.frontend.SlotTemplate;
import org.dse15.wait4op.apigw.session.SessionException;
import org.dse15.wait4op.apigw.session.UserSession;
import org.dse15.wait4op.apigw.util.Constants;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 *
 */
@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SurgeryResource extends BaseResource {

    public SurgeryResource(IAPIFacade APIFacade, UserSession userSession) {
        super(APIFacade, userSession);
    }

    @GET
    @Path("/publicOPSlots")
    public Response getPublicSurgerySlots(){
        try {

            List<Slot> publicSlots = getAPIFacade().getAllSurgerySlots(null);
            return Response.ok(publicSlots).build();

        } catch (Exception e) {
            e.printStackTrace();
            return toResponse(new WebApplicationException(e.getMessage(), e, Response.Status.INTERNAL_SERVER_ERROR));
        }
    }

    @GET
    @Path("/publicOPSlots/{slotId}")
    public Response getPublicSurgerySlotById(@PathParam("slotId")String slotId) {
        try {

            Slot publicSlot = getAPIFacade().getSurgerySlot(null, slotId);
            return Response.ok(publicSlot).build();

        } catch (Exception e) {
            e.printStackTrace();
            return toResponse(new WebApplicationException(e.getMessage(), e, Response.Status.INTERNAL_SERVER_ERROR));
        }
    }

    @GET
    @Path("/myOPSlots")
    public Response getMySurgerySlots(@CookieParam(Constants.AUTH_COOKIE_KEY) String authCookie){

        try {

            List<Slot> publicSlots = getAPIFacade().getAllSurgerySlots(getUserSession().getUser(authCookie));
            return Response.ok(publicSlots).build();

        } catch (SessionException e) {
            return toResponse(new WebApplicationException(e.getMessage(), e, Response.Status.FORBIDDEN));
        } catch (Exception e) {
            e.printStackTrace();
            return toResponse(new WebApplicationException(e.getMessage(), e, Response.Status.INTERNAL_SERVER_ERROR));
        }
    }

    @GET
    @Path("/myOPSlots/{slotId}")
    public Response getMySurgerySlotById(@PathParam("slotId")String slotId,
                                         @CookieParam(Constants.AUTH_COOKIE_KEY) String authCookie) {
        try {

            Slot publicSlot = getAPIFacade().getSurgerySlot(getUserSession().getUser(authCookie), slotId);
            return Response.ok(publicSlot).build();

        } catch (SessionException e) {
            return toResponse(new WebApplicationException(e.getMessage(), e, Response.Status.FORBIDDEN));
        } catch (Exception e) {
            e.printStackTrace();
            return toResponse(new WebApplicationException(e.getMessage(), e, Response.Status.INTERNAL_SERVER_ERROR));
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON) // TODO: JSON (retain parameter names below)
    @Path("/myOPSlots")
    public Response createSurgerySlot(SlotTemplate slotTemplate, @CookieParam(Constants.AUTH_COOKIE_KEY) String authCookie) {
        try {

            Slot slot = getAPIFacade().createSurgerySlot(getUserSession().getUser(authCookie), slotTemplate.getFrom(), slotTemplate.getTo(), slotTemplate.getType());

            return Response.ok(slot).build();

        } catch (SessionException e) {
            return toResponse(new WebApplicationException(e.getMessage(), e, Response.Status.FORBIDDEN));
        } catch (Exception e) {
            e.printStackTrace();
            return toResponse(new WebApplicationException(e.getMessage(), e, Response.Status.INTERNAL_SERVER_ERROR));
        }
    }

    @DELETE
    @Path("/myOPSlots/{slotId}")
    public Response deleteSurgerySlot(@PathParam("slotId")String slotId,
                                      @CookieParam(Constants.AUTH_COOKIE_KEY) String authCookie) {
        try {

            getAPIFacade().deleteSurgerySlot(getUserSession().getUser(authCookie), slotId);
            return Response.ok().build();

        } catch (SessionException e) {
            return toResponse(new WebApplicationException(e.getMessage(), e, Response.Status.FORBIDDEN));
        } catch (Exception e) {
            e.printStackTrace();
            return toResponse(new WebApplicationException(e.getMessage(), e, Response.Status.INTERNAL_SERVER_ERROR));
        }
    }
}
