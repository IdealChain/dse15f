package org.dse15.wait4op.apigw.util;

/**
 *
 */
public final class Constants {

    public static final String APP_NAME = "API-Gateway";
    public static final String AUTH_COOKIE_KEY = "wait4op_session";
}
