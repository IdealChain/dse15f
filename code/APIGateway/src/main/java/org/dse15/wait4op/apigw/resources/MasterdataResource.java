package org.dse15.wait4op.apigw.resources;

import org.dse15.wait4op.apigw.core.interfaces.IAPIFacade;
import org.dse15.wait4op.apigw.model.User;
import org.dse15.wait4op.apigw.session.SessionException;
import org.dse15.wait4op.apigw.session.UserSession;
import org.dse15.wait4op.apigw.util.Constants;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 *
 */
@Path("/data")
@Produces(MediaType.APPLICATION_JSON)
public class MasterdataResource extends BaseResource {

    public MasterdataResource(IAPIFacade apiFacade, UserSession userSession) {
        super(apiFacade, userSession);
    }

    @GET
    @Path("/patients")
    public Response getPatients(@CookieParam(Constants.AUTH_COOKIE_KEY) String authCookie) {

        try {

            List<User> patients = getAPIFacade().getPatients(getUserSession().getUser(authCookie));
            return Response.ok(patients).build();

        } catch (SessionException e) {
            return toResponse(new WebApplicationException(e.getMessage(), e, Response.Status.FORBIDDEN));
        } catch (Exception e) {
            e.printStackTrace();
            return toResponse(new WebApplicationException(e.getMessage(), e, Response.Status.INTERNAL_SERVER_ERROR));
        }
    }

    @GET
    @Path("/slotTypes")
    public Response getSlotTypes(@CookieParam(Constants.AUTH_COOKIE_KEY) String authCookie) {
        try {

            List<String> slotTypes = getAPIFacade().getSlotTypes(getUserSession().getUser(authCookie));
            return Response.ok(slotTypes).build();

        } catch (SessionException e) {
            return toResponse(new WebApplicationException(e.getMessage(), e, Response.Status.FORBIDDEN));
        } catch (Exception e) {
            e.printStackTrace();
            return toResponse(new WebApplicationException(e.getMessage(), e, Response.Status.INTERNAL_SERVER_ERROR));
        }
    }
}
