package org.dse15.wait4op.apigw.core.interfaces;

import org.dse15.wait4op.apigw.core.exception.RemoteMicroServiceException;

/**
 *
 */
public interface IClient {
    String getResponse(String url, String content, String method) throws RemoteMicroServiceException;
}
