package org.dse15.wait4op.apigw.health;

import com.codahale.metrics.health.HealthCheck;
import org.apache.http.client.HttpClient;

/**
 * Checks if the remote client (which is used to connect to the microservices) is still up
 * and running
 */
public class RemoteClientHealthCheck extends HealthCheck {

    private HttpClient _client;

    public RemoteClientHealthCheck(HttpClient client) {
        _client = client;
    }

    @Override
    protected Result check() throws Exception {
        if (_client == null)
            return Result.unhealthy("HttpClient is null");

        return Result.healthy();
    }
}
