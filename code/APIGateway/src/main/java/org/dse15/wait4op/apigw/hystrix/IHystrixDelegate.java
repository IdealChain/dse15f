package org.dse15.wait4op.apigw.hystrix;

/**
 *
 */
public interface IHystrixDelegate<T> {

    T run() throws Exception;
}