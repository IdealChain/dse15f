package org.dse15.wait4op.apigw.model.backend;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 */
public class Reservation {

    private String _slotId;
    private String _patientId;
    private String _doctorId;

    @JsonProperty
    public String getSlotId() {
        return _slotId;
    }

    public void setSlotId(String slotId) {
        _slotId = slotId;
    }

    @JsonProperty
    public String getPatientId() {
        return _patientId;
    }

    public void setPatientId(String patientId) {
        _patientId = patientId;
    }

    @JsonProperty
    public String getDoctorId() {
        return _doctorId;
    }

    public void setDoctorId(String doctorId) {
        _doctorId = doctorId;
    }
}
