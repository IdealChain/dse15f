package org.dse15.wait4op.apigw.model.frontend;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 *
 */
public class Slot {

    private String _id;
    private Date _from;
    private Date _to;
    private String _type;
    private String _clinicName;
    private String _doctorName;
    private String _patientName;

    @JsonProperty
    public String getId() {
        return _id;
    }

    public void setId(String id) {
        _id = id;
    }

    @JsonProperty
    public Date getFrom() {
        return _from;
    }

    public void setFrom(Date from) {
        _from = from;
    }

    @JsonProperty
    public Date getTo() {
        return _to;
    }

    public void setTo(Date to) {
        _to = to;
    }

    @JsonProperty
    public String getType() {
        return _type;
    }

    public void setType(String type) {
        _type = type;
    }

    @JsonProperty("clinic")
    public String getClinicName() {
        return _clinicName;
    }

    public void setClinicName(String clinicName) {
        _clinicName = clinicName;
    }

    @JsonProperty("doctor")
    public String getDoctorName() {
        return _doctorName;
    }

    public void setDoctorName(String doctorName) {
        _doctorName = doctorName;
    }

    @JsonProperty("patient")
    public String getPatientName() {
        return _patientName;
    }

    public void setPatientName(String patientName) {
        _patientName = patientName;
    }
}
