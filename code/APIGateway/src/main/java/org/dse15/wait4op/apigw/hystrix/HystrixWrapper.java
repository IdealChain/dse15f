package org.dse15.wait4op.apigw.hystrix;

import com.netflix.hystrix.HystrixCommand;
import org.dse15.wait4op.apigw.core.exception.APIGatewayException;
import org.dse15.wait4op.apigw.core.exception.RemoteMicroServiceException;

import java.util.HashMap;

/**
 *
 */
public class HystrixWrapper {

    public static int TIMEOUT = 1500;

    public static String execute(String depName, IHystrixDelegate<String> delegate) throws RemoteMicroServiceException, APIGatewayException {
        HystrixCommandDelegate<String> hystrixCommandDelegate = new HystrixCommandDelegate<>(depName, delegate, TIMEOUT);
        return hystrixCommandDelegate.execute();
    }

    public static String execute(String depName, IHystrixDelegateWithFallback<String> delegate) throws RemoteMicroServiceException, APIGatewayException {
        HystrixCommandDelegateWithFallback<String> hystrixCommandDelegate = new HystrixCommandDelegateWithFallback<>(depName, delegate, TIMEOUT);
        return hystrixCommandDelegate.execute();
    }
}
