package org.dse15.wait4op.apigw.core;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.hystrix.exception.HystrixRuntimeException;
import org.dse15.wait4op.apigw.core.exception.APIGatewayException;
import org.dse15.wait4op.apigw.core.exception.NotFoundException;
import org.dse15.wait4op.apigw.core.exception.RemoteMicroServiceException;
import org.dse15.wait4op.apigw.core.interfaces.IAPIFacade;
import org.dse15.wait4op.apigw.core.interfaces.IClient;
import org.dse15.wait4op.apigw.hystrix.HystrixWrapper;
import org.dse15.wait4op.apigw.hystrix.IHystrixDelegate;
import org.dse15.wait4op.apigw.hystrix.IHystrixDelegateWithFallback;
import org.dse15.wait4op.apigw.model.User;
import org.dse15.wait4op.apigw.model.backend.Reservation;
import org.dse15.wait4op.apigw.model.backend.SlotEntry;
import org.dse15.wait4op.apigw.model.frontend.ReservationRequest;
import org.dse15.wait4op.apigw.model.frontend.Slot;
import org.dse15.wait4op.apigw.session.InsufficientAuthorizationException;
import org.dse15.wait4op.apigw.session.SessionException;

import javax.ws.rs.HttpMethod;
import java.net.URL;
import java.util.*;

/**
 *
 */
class APIFacade implements IAPIFacade {

    public static final String MS_KLINISYS = "klinisys";
    public static final String MS_RESERVATION = "reservation";
    public static final String MS_OPMATCHER = "opmatcher";
    public static final String MS_NOTIFIER = "notifier";

    private Map<String, URL> _microservices;
    private DataAggregator _dataAggregator;
    private IClient _client;

    public APIFacade() {
        _microservices = new HashMap<>();
        _dataAggregator = new DataAggregator();
    }

    /**
     * Registers the given microservices
     */
    public void registerMicroservices(Map<String, URL> microservices) {
        for (String key : microservices.keySet())
            _microservices.put(key, microservices.get(key));
    }

    private URL getMicroserviceURL(String name) {
        return _microservices.get(name);
    }

    /**
     * Registers the given Client for remote connectivity. Only *one* client can be registered.
     * Overwrites the previous one.
     *
     * @param client
     */
    @Override
    public void registerClient(IClient client) {
        _client = client;
    }

    /**
     * Gets all users from the KLINIsys microservice
     * @return
     * @throws RemoteMicroServiceException
     * @throws APIGatewayException
     */
    public List<User> getUsers() throws RemoteMicroServiceException, APIGatewayException {
        try {
            String result = HystrixWrapper.execute(MS_KLINISYS, new IHystrixDelegateWithFallback<String>() {
                @Override
                public String run() throws Exception {
                    return _client.getResponse(
                            String.format("%s/masterdata", getMicroserviceURL(MS_KLINISYS).toExternalForm()),
                            null, HttpMethod.GET);
                }

                @Override
                public String getFallback() {
                    return "[]";
                }
            });

            return _dataAggregator.parseUsers(result);

        } catch (HystrixRuntimeException e) {
            throw new RemoteMicroServiceException(e.getCause().getMessage(), e);
        }
    }

    /**
     * Gets the corresponding users to the given username/password from the KLINIsys microservice
     * @param username
     * @param password
     * @return
     * @throws RemoteMicroServiceException
     * @throws APIGatewayException
     */
    @Override
    public User getUser(final String username, final String password) throws RemoteMicroServiceException, APIGatewayException {
        try {
            String result = HystrixWrapper.execute(MS_KLINISYS, new IHystrixDelegate<String>() {
                @Override
                public String run() throws Exception {
                    return _client.getResponse(
                            String.format("%s/masterdata/login?username=%s&password=%s", getMicroserviceURL(MS_KLINISYS).toExternalForm(), username, password),
                            null, HttpMethod.GET);
                }
            });

            return _dataAggregator.parseUser(result);
        } catch (HystrixRuntimeException e) {
            throw new RemoteMicroServiceException(e.getCause().getMessage(), e);
        }
    }

    /**
     * Gets the correspondig user by id
     * @param userId
     * @return null if not found
     * @throws RemoteMicroServiceException
     * @throws APIGatewayException
     */
    @Override
    public User getUser(String userId) throws RemoteMicroServiceException, APIGatewayException {
        if (userId == null) {
            throw new APIGatewayException("userId must not be null.");
        }

        try {
            String result = HystrixWrapper.execute(MS_KLINISYS, new IHystrixDelegate<String>() {
                @Override
                public String run() throws Exception {
                    return _client.getResponse(
                            String.format("%s/masterdata/%s", getMicroserviceURL(MS_KLINISYS).toExternalForm(), userId), null, HttpMethod.GET);
                }
            });

            return _dataAggregator.parseUser(result);

        } catch (HystrixRuntimeException e) {
            throw new RemoteMicroServiceException(e.getCause().getMessage(), e);
        }
    }

    @Override
    public List<User> getPatients(User user) throws RemoteMicroServiceException, APIGatewayException, SessionException {
        if (user.getUserType() != User.UserType.DOCTOR) {
            throw new InsufficientAuthorizationException("Only doctors can request a list of patients.");
        }

        try {
            String result = HystrixWrapper.execute(MS_KLINISYS, new IHystrixDelegate<String>() {
                @Override
                public String run() throws Exception {
                    return _client.getResponse(
                            String.format("%s/masterdata/type?type=PATIENT", getMicroserviceURL(MS_KLINISYS).toExternalForm()), null, HttpMethod.GET);
                }
            });

            return _dataAggregator.parseUsers(result);

        } catch (HystrixRuntimeException e) {
            throw new RemoteMicroServiceException(e.getCause().getMessage(), e);
        }
    }

    /**
     * Gets all surgery slots, the given user is allowed to view, from the KLINIsys and RESERvation microservice
     * @param user
     * @return
     * @throws RemoteMicroServiceException
     * @throws APIGatewayException
     */
    @Override
    public List<Slot> getAllSurgerySlots(User user) throws RemoteMicroServiceException, APIGatewayException {
        try {
            String slotEntryResults = HystrixWrapper.execute(MS_KLINISYS, new IHystrixDelegate<String>() {
                @Override
                public String run() throws Exception {
                    return _client.getResponse(
                            String.format("%s/slots", getMicroserviceURL(MS_KLINISYS).toExternalForm()), null, HttpMethod.GET);
                }
            });
            List<SlotEntry> slotEntries = _dataAggregator.parseSlotEntries(slotEntryResults);

            List<User> users = getUsers();

            List<String> slotIds = new ArrayList<>();
            slotEntries.stream().forEach(s -> slotIds.add(s.getSlotId()));
            StringJoiner sj = new StringJoiner("&ids=");
            slotIds.forEach(sid -> sj.add(sid));

            String reservationResult = HystrixWrapper.execute(MS_RESERVATION, new IHystrixDelegateWithFallback<String>() {
                @Override
                public String run() throws Exception {
                    return _client.getResponse(
                            String.format("%s/reservation?ids=%s", getMicroserviceURL(MS_RESERVATION).toExternalForm(), sj.toString()),
                            null, HttpMethod.GET);
                }
                @Override
                public String getFallback() {
                    return "[]";
                }
            });
            List<Reservation> reservations = _dataAggregator.parseReservations(reservationResult);

            return _dataAggregator.enrich(slotEntries, reservations, users, user);

        } catch (HystrixRuntimeException e) {
            throw new RemoteMicroServiceException(e.getCause().getMessage(), e);
        }
    }

    /**
     * Gets the surgery slot with the given id
     * @param user
     * @param slotId
     * @return null if not found
     * @throws NotFoundException
     * @throws RemoteMicroServiceException
     */
    @Override
    public Slot getSurgerySlot(User user, String slotId) throws RemoteMicroServiceException, APIGatewayException {
        if (slotId == null) {
            throw new APIGatewayException("SlotId must not be null.");
        }

        try {
            String slotEntryResult = HystrixWrapper.execute(MS_KLINISYS, new IHystrixDelegate<String>() {
                @Override
                public String run() throws Exception {
                    return _client.getResponse(
                            String.format("%s/slots/%s", getMicroserviceURL(MS_KLINISYS).toExternalForm(), slotId), null, HttpMethod.GET.toString());
                }
            });
            SlotEntry slotEntry = _dataAggregator.parseSlot(slotEntryResult);

            List<User> users = getUsers();

            String reservationResult = HystrixWrapper.execute(MS_RESERVATION, new IHystrixDelegate<String>() {
                @Override
                public String run() throws Exception {
                    return _client.getResponse(
                            String.format("%s/reservation?ids[]=%s", getMicroserviceURL(MS_RESERVATION).toExternalForm(), slotEntry.getSlotId()),
                            null, HttpMethod.GET.toString());
                }
            });
            List<Reservation> reservations = _dataAggregator.parseReservations(reservationResult);

            List<Slot> slots = _dataAggregator.enrich(new ArrayList<SlotEntry>() {{add(slotEntry); }}, reservations, users, user);

            return slots.get(0);

        } catch (HystrixRuntimeException e) {
            throw new RemoteMicroServiceException(e.getCause().getMessage(), e);
        }
    }

    @Override
    public Slot createSurgerySlot(User user, long from, long to, String type) throws APIGatewayException, RemoteMicroServiceException, SessionException {
        if (user.getUserType() != User.UserType.CLINIC) {
            throw new InsufficientAuthorizationException("Surgery-Slots can only be created by a clinic-user.");
        }

        SlotEntry slotEntry = new SlotEntry();
        slotEntry.setType(type);
        slotEntry.setFrom(new Date(from));
        slotEntry.setTo(new Date(to));
        slotEntry.setClinicId(user.getId());

        try {
            String slotIdResult = HystrixWrapper.execute(MS_KLINISYS, new IHystrixDelegate<String>() {
                @Override
                public String run() throws Exception {
                    return _client.getResponse(
                            String.format("%s/slots", getMicroserviceURL(MS_KLINISYS).toExternalForm()), _dataAggregator.serializeSlotTemplate(slotEntry), HttpMethod.POST);
                }
            });

            String slotId = _dataAggregator.parseId(slotIdResult);

            return getSurgerySlot(user, slotId);

        } catch (HystrixRuntimeException e) {
            throw new RemoteMicroServiceException(e.getCause().getMessage(), e);
        }
    }

    @Override
    public void deleteSurgerySlot(User user, String slotId) throws RemoteMicroServiceException, APIGatewayException, SessionException {
        if (slotId == null) {
            throw new APIGatewayException("SlotId must not be null.");
        }

        if (user.getUserType() != User.UserType.CLINIC) {
            throw new InsufficientAuthorizationException("Surgery-Slots can only be deleted by a clinic-user.");
        }

        try {
            String result = HystrixWrapper.execute(MS_RESERVATION, new IHystrixDelegate<String>() {
                @Override
                public String run() throws Exception {
                    return _client.getResponse(
                            String.format("%s/slots/%s", getMicroserviceURL(MS_RESERVATION).toExternalForm(), slotId), null, HttpMethod.DELETE);
                }
            });

        } catch (HystrixRuntimeException e) {
            throw new RemoteMicroServiceException(e.getCause().getMessage(), e);
        }
    }

    @Override
    public void requestReservation(User user, ReservationRequest reservationRequest) throws RemoteMicroServiceException, APIGatewayException, SessionException {
        if (user.getUserType() != User.UserType.DOCTOR) {
            throw new InsufficientAuthorizationException("Reservation request can only be created by a doctor.");
        }

        reservationRequest.setDoctorId(user.getId());

        try {

            final ObjectMapper mapper = new ObjectMapper();
            String result = HystrixWrapper.execute(MS_OPMATCHER, new IHystrixDelegate<String>() {
                @Override
                public String run() throws Exception {
                    return _client.getResponse(
                            String.format("%s/requestReservation", getMicroserviceURL(MS_OPMATCHER).toExternalForm()),
                            mapper.writeValueAsString(reservationRequest),
                            HttpMethod.POST);
                }
            });

        } catch (HystrixRuntimeException e) {
            throw new RemoteMicroServiceException(e.getCause().getMessage(), e);
        }
    }

    @Override
    public void deteteReservation(User user, String slotId) throws RemoteMicroServiceException, APIGatewayException, SessionException {
        if (slotId == null) {
            throw new APIGatewayException("SlotId must not be null.");
        }

        if (user.getUserType() != User.UserType.DOCTOR) {
            throw new InsufficientAuthorizationException("Reservations can only be deleted by a doctor.");
        }

        try {
            String result = HystrixWrapper.execute(MS_RESERVATION, new IHystrixDelegate<String>() {
                @Override
                public String run() throws Exception {
                    return _client.getResponse(
                            String.format("%s/reservation/%s", getMicroserviceURL(MS_RESERVATION).toExternalForm(), slotId),
                            null, HttpMethod.DELETE);
                }
            });

        } catch (HystrixRuntimeException e) {
            throw new RemoteMicroServiceException(e.getCause().getMessage(), e);
        }
    }

    @Override
    public List<String> getSlotTypes(User user) throws RemoteMicroServiceException, APIGatewayException, SessionException {
        if (user.getUserType() != User.UserType.CLINIC && user.getUserType() != User.UserType.DOCTOR) {
            throw new InsufficientAuthorizationException("SlotTypes can only be requested by a clinic- or doctor-user.");
        }

        try {
            String result = HystrixWrapper.execute(MS_KLINISYS, new IHystrixDelegate<String>() {
                @Override
                public String run() throws Exception {
                    return _client.getResponse(
                            String.format("%s/slots/slotTypes", getMicroserviceURL(MS_KLINISYS).toExternalForm()),
                            null, HttpMethod.GET);
                }
            });

            return _dataAggregator.parseSlotTypes(result);

        } catch (HystrixRuntimeException e) {
            throw new RemoteMicroServiceException(e.getCause().getMessage(), e);
        }
    }

    @Override
    public String getNotifications(User user, String since) throws RemoteMicroServiceException, APIGatewayException, SessionException {

        final String url = String.format("%s/notifications/%s%s",
                getMicroserviceURL(MS_NOTIFIER).toExternalForm(),
                user.getId(),
                (since != null ? "?since=" + since : ""));

        try {
            String result = HystrixWrapper.execute(MS_NOTIFIER, new IHystrixDelegate<String>() {
                @Override
                public String run() throws Exception {
                    return _client.getResponse(url, null, HttpMethod.GET);
                }
            });

            return result;

        } catch (HystrixRuntimeException e) {
            throw new RemoteMicroServiceException(e.getCause().getMessage(), e);
        }
    }
}
