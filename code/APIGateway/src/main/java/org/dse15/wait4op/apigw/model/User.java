package org.dse15.wait4op.apigw.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
@JsonPropertyOrder({ "id", "username", "fullName", "role", "properties" })
public class User {

    private String _id;
    private String _username;
    private UserType _userType;
    private String _fullName;
    private Map<String, String> _properties = new HashMap<>();

    /**
     * @return the public identifier of this user (used to query agains the API-Gateway)
     */
    @JsonProperty("id")
    public String getId() {
        return _id;
    }

    public void setId(String id) {
        _id = id;
    }

    /**
     * @return the username of this user.
     */
    @JsonProperty("username")
    public String getUsername() {
        return _username;
    }

    public void setUsername(String username) {
        _username = username;
    }

    /**
     * @return the type of user (PATIENT, DOCTOR or CLINIC)
     */
    @JsonProperty("role")
    public UserType getUserType() {
        return _userType;
    }

    public void setUserType(UserType userType) {
        _userType = userType;
    }

    @JsonProperty("fullname")
    public String getFullName() {
        return _fullName;
    }

    public void setFullName(String fullName) {
        _fullName = fullName;
    }

    /**
     * @return Key-Value-List of unstructered properties assigned to this user (e.g. geo-data for CLINIC and PATIENT...)
     */
    @JsonProperty
    public Map<String, String> getProperties() {
        return _properties;
    }

    /**
     * Type definition of the different user types
     */
    public enum  UserType {
        PATIENT,
        DOCTOR,
        CLINIC
    }
}