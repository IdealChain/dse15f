package org.dse15.wait4op.apigw.core;

import com.netflix.hystrix.HystrixCommand;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.BasicHttpEntity;
import org.dse15.wait4op.apigw.core.exception.RemoteMicroServiceException;
import org.dse15.wait4op.apigw.core.interfaces.IClient;

import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 */
class RemoteClient implements IClient {

    private HttpClient _httpClient;

    public RemoteClient(HttpClient httpClient) {
        _httpClient = httpClient;
    }

    @Override
    public String getResponse(String url, String content, String method) throws RemoteMicroServiceException {

        try {
            HttpUriRequest httpRequest;
            switch (method.toLowerCase()) {
                case "post":
                    BasicHttpEntity payload = new BasicHttpEntity();
                    payload.setContent(new ByteArrayInputStream(content.getBytes()));

                    httpRequest = new HttpPost(url);
                    httpRequest.addHeader("Content-Type", "application/json");
                    ((HttpPost) httpRequest).setEntity(payload);
                    break;
                case "get":
                    httpRequest = new HttpGet(url);
                    break;
                case "delete":
                    httpRequest = new HttpDelete(url);
                    break;
                default:
                    throw new IllegalArgumentException(String.format("The given method '%s' is not supported", method));
            }

            // TODO: don't ignore invalid response status here!
            // YES master.
            HttpResponse response = _httpClient.execute(httpRequest);
            int code = response.getStatusLine().getStatusCode();
            if (code != Response.Status.OK.getStatusCode()) {
                throw new RemoteMicroServiceException("Status Code was " + code + " but expected 200");
            }

            StringBuilder responseStringBuilder = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = null;
            while ((line = br.readLine()) != null) {
                responseStringBuilder.append(line);
            }
            br.close();

            return responseStringBuilder.toString();
        } catch (IOException e) {
            throw new RemoteMicroServiceException(e.getMessage(), e);
        }
    }
}
