package org.dse15.wait4op.apigw.core;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.dse15.wait4op.apigw.core.exception.APIGatewayException;
import org.dse15.wait4op.apigw.model.User;
import org.dse15.wait4op.apigw.model.backend.Reservation;
import org.dse15.wait4op.apigw.model.backend.SlotEntry;
import org.dse15.wait4op.apigw.model.frontend.Slot;

import java.io.IOException;
import java.io.StringReader;
import java.util.*;

/**
 *
 */
public class DataAggregator {

    private static String NOT_AVAILABLE = "N/A";

    private static final String USER_ID = "id";
    private static final String USER_US = "loginName";
    private static final String USER_FN = "firstName";
    private static final String USER_LN = "lastName";
    private static final String USER_TI = "title";
    private static final String USER_TY = "type";

    private static final String SLOTENTRY_ID = "id";
    private static final String SLOTENTRY_FROM = "from";
    private static final String SLOTENTRY_TO = "to";
    private static final String SLOTENTRY_TYPE = "type";
    private static final String SLOTENTRY_HOSPITALID = "hospitalId";

    private static final String RESERVATION_SLOTID = "slotId";
    private static final String RESERVATION_DOCTORID = "doctorId";
    private static final String RESERVATION_PATIENTID = "patientId";

    private ObjectMapper _objectMapper = new ObjectMapper();

    /**
     * Converts the given User-JSON into a User-Object
     *
     * @param result
     * @return
     * @throws IOException
     */
    public User parseUser(String result) throws APIGatewayException {

        try {
            StringReader sr = new StringReader(result);
            JsonNode jsonUser = _objectMapper.readTree(sr);
            if (jsonUser.get(USER_ID) == null)
                return null;

            User user = new User();
            user.setId(jsonUser.get(USER_ID).asText());
            user.setUsername(jsonUser.get(USER_US).asText());
            user.setUserType(parseUserType(jsonUser.get(USER_TY).asText()));
            user.setFullName(String.format("%s %s %s",
                    jsonUser.get(USER_TI) != null ? jsonUser.get(USER_TI).asText() : "",
                    jsonUser.get(USER_FN) != null ? jsonUser.get(USER_FN).asText() : "",
                    jsonUser.get(USER_LN) != null ? jsonUser.get(USER_LN).asText() : "").trim());
            if (user.getFullName().isEmpty())
                user.setFullName(String.format("(%s)", user.getUsername()));

            return user;
        } catch (IOException e) {
            throw new APIGatewayException(e.getMessage(), e);
        }
    }

    public List<User> parseUsers(String result) throws APIGatewayException {

        try {
            StringReader sr = new StringReader(result);
            JsonNode jsonUsers = _objectMapper.readTree(sr);
            if (jsonUsers.size() == 0) {
                return Collections.<User>emptyList();
            }

            if (!jsonUsers.isArray()) {
                throw new APIGatewayException("Cannot parse jsonUsers-Array. Input string was not a json-array.");
            }

            List<User> users = new ArrayList<>();
            for(JsonNode jsonUser : jsonUsers) {
                try {
                    users.add(parseUser(jsonUser.toString()));
                } catch (Exception e) {
                    System.out.println("ERROR while parsing user (will be ignored): " + e.getMessage());
                }
            }

            return users;

        } catch (IOException e) {
            throw new APIGatewayException(e.getMessage(), e);
        }
    }

    public User.UserType parseUserType(String userType) {
        if (userType == null)
            return null;

        switch (userType.toUpperCase()) {
            case "PATIENT": return User.UserType.PATIENT;
            case "DOCTOR": return User.UserType.DOCTOR;
            case "HOSPITAL": return User.UserType.CLINIC;
        }
        return null;
    }

    public List<SlotEntry> parseSlotEntries(String result) throws APIGatewayException {

        try {
            StringReader sr = new StringReader(result);
            JsonNode jsonSlotEntries = _objectMapper.readTree(sr);
            if (jsonSlotEntries.size() == 0) {
                return Collections.<SlotEntry>emptyList();
            }
            if (!jsonSlotEntries.isArray()) {
                throw new APIGatewayException("Cannot parse jsonSlot-Array. Input string was not a json-array.");
            }

            List<SlotEntry> slotEntries = new ArrayList<>();
            for(JsonNode jsonSlotEntry : jsonSlotEntries) {
                try {
                    slotEntries.add(parseSlot(jsonSlotEntry.toString()));
                } catch (Exception e) {
                    System.out.println("ERROR while parsing slotEntry (will be ignored): " + e.getMessage());
                }
            }

            return slotEntries;

        } catch (IOException e) {
            throw new APIGatewayException(e.getMessage(), e);
        }
    }

    public SlotEntry parseSlot(String slotResult) throws APIGatewayException {
        try {
            StringReader sr = new StringReader(slotResult);
            JsonNode jsonSlotEntry = _objectMapper.readTree(sr);
            if (jsonSlotEntry.get(SLOTENTRY_ID) == null)
                return null;

            SlotEntry se = new SlotEntry();
            se.setSlotId(jsonSlotEntry.get(SLOTENTRY_ID).asText());
            se.setFrom(new Date(Long.valueOf(jsonSlotEntry.get(SLOTENTRY_FROM).asText())));
            se.setTo(new Date(Long.valueOf(jsonSlotEntry.get(SLOTENTRY_TO).asText())));
            se.setType(jsonSlotEntry.get(SLOTENTRY_TYPE).asText());
            se.setClinicId(jsonSlotEntry.get(SLOTENTRY_HOSPITALID).asText());

            return se;
        } catch(IOException e) {
            throw new APIGatewayException(e.getMessage(), e);
        }
    }

    public List<Reservation> parseReservations(String reservationResult) throws APIGatewayException {
        try {
            StringReader sr = new StringReader(reservationResult);
            JsonNode jsonReservations = _objectMapper.readTree(sr);
            if (jsonReservations.size() == 0) {
                return Collections.<Reservation>emptyList();
            }

            if (!jsonReservations.isArray()) {
                throw new APIGatewayException("Cannot parse jsonReservation-Array. Input string was not a json-array.");
            }

            List<Reservation> reservations = new ArrayList<>();
            for(JsonNode jsonReservation : jsonReservations) {

                Reservation r = new Reservation();
                r.setSlotId(jsonReservation.get(RESERVATION_SLOTID).asText());
                r.setDoctorId(jsonReservation.get(RESERVATION_DOCTORID).asText());
                r.setPatientId(jsonReservation.get(RESERVATION_PATIENTID).asText());
                reservations.add(r);
            }

            return reservations;

        } catch (IOException e) {
            throw new APIGatewayException(e.getMessage(), e);
        }
    }

    public List<Slot> enrich(List<SlotEntry> slotEntries, List<Reservation> reservations, List<User> users, User requestingUser) {

        List<Slot> slots = new ArrayList<>(slotEntries.size());

        for (SlotEntry slotEntry : slotEntries) {
            Slot slot = new Slot();
            slot.setId(slotEntry.getSlotId());
            slot.setFrom(slotEntry.getFrom());
            slot.setTo(slotEntry.getTo());
            slot.setType(slotEntry.getType());

            Optional<Reservation> reservation = reservations.stream().filter(r -> r.getSlotId().equals(slotEntry.getSlotId())).findFirst();
            if (reservation.isPresent()) {
                Optional<User> doctor = users.stream().filter(u -> u.getId().equals(reservation.get().getDoctorId())).findFirst();
                slot.setDoctorName(doctor.isPresent() ? doctor.get().getFullName() : NOT_AVAILABLE);

                if (requestingUser != null) {
                    Optional<User> patient = users.stream().filter(u -> u.getId().equals(reservation.get().getPatientId())).findFirst();
                    slot.setPatientName(patient.isPresent() ? patient.get().getFullName() : NOT_AVAILABLE);

//                    if (requestingUser.getUserType() == User.UserType.DOCTOR && !requestingUser.getId().equals(reservation.get().getDoctorId()))
//                        continue;       //doctor is logged in, only collect relevant slots
//                    if (requestingUser.getUserType() == User.UserType.PATIENT && !requestingUser.getId().equals(reservation.get().getPatientId()))
//                        continue;
                }
            }

            if (skipSlot(requestingUser, reservation.isPresent() ? reservation.get() : null)) {
                continue;
            }

            Optional<User> clinic = users.stream().filter(u -> u.getId().equals(slotEntry.getClinicId())).findFirst();
            slot.setClinicName(clinic.isPresent() ? clinic.get().getFullName() : NOT_AVAILABLE);

            slots.add(slot);
        }

        return slots;
    }

    public List<String> parseSlotTypes(String result) throws APIGatewayException {

        try {
            StringReader sr = new StringReader(result);
            JsonNode jsonSlotTypes = _objectMapper.readTree(sr);
            if (jsonSlotTypes.size() == 0) {
                return Collections.<String>emptyList();
            }

            if (!jsonSlotTypes.isArray()) {
                throw new APIGatewayException("Cannot parse jsonSlotType-Array. Input string was not a json-array.");
            }

            List<String> slotTypes = new ArrayList<>();
            for(JsonNode jsonSlotType : jsonSlotTypes) {
                slotTypes.add(jsonSlotType.asText());
            }

            return slotTypes;

        } catch (IOException e) {
            throw new APIGatewayException(e.getMessage(), e);
        }

    }

    public String serializeSlotTemplate(SlotEntry slotEntry) throws APIGatewayException {

        try {
            JsonNodeFactory nodeFactory = JsonNodeFactory.instance;
            ObjectNode slot = nodeFactory.objectNode();
            slot.put("hospitalId", slotEntry.getClinicId());
            slot.put("from", Long.toString(slotEntry.getFrom().getTime()));
            slot.put("to", Long.toString(slotEntry.getTo().getTime()));
            slot.put("type", slotEntry.getType());

            return slot.toString();

        } catch (NullPointerException e) {
            throw new APIGatewayException(e.getMessage(), e);
        }
    }

    public String parseId(String slotIdResult) throws APIGatewayException {

        try {
            StringReader sr = new StringReader(slotIdResult);
            JsonNode jsonId = _objectMapper.readTree(sr);

            return jsonId.get("id").asText();

        } catch (IOException e) {
            throw new APIGatewayException(e.getMessage(), e);
        }
    }

    private boolean skipSlot(User requestingUser, Reservation reservation) {
        //PUBLIC view, show *all* rows
        if (requestingUser == null)
            return false;

        //DOCTOR view, only show reserved slots
        if (requestingUser.getUserType() == User.UserType.DOCTOR && reservation == null)
            return true;
        //DOCTOR view, only show slots to which they are assigned
        else if(requestingUser.getUserType() == User.UserType.DOCTOR && reservation != null && !reservation.getDoctorId().equals(requestingUser.getId()))
            return true;

        //PATIENT view, only show reserved slots
        if (requestingUser.getUserType() == User.UserType.PATIENT && reservation == null)
            return true;
        //PATIENT view, only show slots to which they are assigned
        else if(requestingUser.getUserType() == User.UserType.PATIENT && reservation != null && !reservation.getPatientId().equals(requestingUser.getId()))
            return true;

        return false;
    }
}
