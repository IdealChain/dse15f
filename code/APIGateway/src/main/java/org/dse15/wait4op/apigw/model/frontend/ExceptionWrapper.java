package org.dse15.wait4op.apigw.model.frontend;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * http://jsonapi.org/format/#error-objects
 */
public class ExceptionWrapper {

    private int _statusCode;
    private String _message;

    public ExceptionWrapper(int statusCode, String message) {
        _statusCode = statusCode;
        _message = message;
    }

    @JsonProperty("status")
    public int getStatusCode() {
        return _statusCode;
    }

    @JsonProperty("status")
    public void setStatusCode(int statusCode) {
        _statusCode = statusCode;
    }

    @JsonProperty("title")
    public String getMessage() {
        return _message;
    }

    @JsonProperty("title")
    public void setMessage(String message) {
        _message = message;
    }
}
