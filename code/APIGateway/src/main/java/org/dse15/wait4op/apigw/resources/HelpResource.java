package org.dse15.wait4op.apigw.resources;

import org.dse15.wait4op.apigw.util.Constants;
import org.jvnet.hk2.annotations.Optional;

import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 */
@Path("/help")
@Produces(MediaType.TEXT_PLAIN)
public class HelpResource {

    private List<String> _endPoints = new ArrayList<>();

    public void init(String rawEndpoints) {
        if (rawEndpoints == null)
            return;

        // https://www.regex101.com/
        Pattern pattern = Pattern.compile("[A-Z]{3,}\\s+[a-zA-Z\\/\\{\\}]+");
        for (String rawEndpoint : rawEndpoints.split(System.lineSeparator())) {
            Matcher matcher = pattern.matcher(rawEndpoint);
            if (matcher.find()) {
                _endPoints.add(matcher.group());
            }
        }

        //
        // Sort 1. by path, 2. by HTTP-Method
        Collections.sort(_endPoints, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                String[] o1arr = o1.split("\\s+");
                String[] o2arr = o2.split("\\s+");

                if (o1arr.length != 2 && o2arr.length != 2)
                    return o1.compareTo(o2);

                int pathCompare = o1arr[1].compareTo(o2arr[1]);
                if (pathCompare != 0)
                    return pathCompare;
                else
                    return o1arr[0].compareTo(o1arr[0]);
            }
        });
    }

    @GET
    public Response getSurgerySlots(@Optional @CookieParam(Constants.AUTH_COOKIE_KEY) String authCookie){
        StringBuilder sb = new StringBuilder();
        sb.append("Authentication-Cookie: " + authCookie);
        sb.append(System.lineSeparator());

        for (String endpoint : _endPoints) {
            sb.append(endpoint);
            sb.append(System.lineSeparator());
        }

        return Response.ok(sb.toString()).build();
    }
}