package org.dse15.wait4op.apigw.model.backend;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 *
 */
public class SlotEntry {

    private String _slotId;
    private Date _from;
    private Date _to;
    private String _type;
    private String _clinicId;

    @JsonProperty("id")
    public String getSlotId() {
        return _slotId;
    }

    public void setSlotId(String slotId) {
        _slotId = slotId;
    }

    @JsonProperty()
    public Date getFrom() {
        return _from;
    }

    public void setFrom(Date from) {
        _from = from;
    }

    @JsonProperty()
    public Date getTo() {
        return _to;
    }

    public void setTo(Date to) {
        _to = to;
    }

    @JsonProperty()
    public String getType() {
        return _type;
    }

    public void setType(String type) {
        _type = type;
    }

    @JsonProperty()
    public String getClinicId() {
        return _clinicId;
    }

    public void setClinicId(String clinicId) {
        _clinicId = clinicId;
    }
}
