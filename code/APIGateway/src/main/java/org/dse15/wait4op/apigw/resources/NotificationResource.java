package org.dse15.wait4op.apigw.resources;

import org.dse15.wait4op.apigw.core.interfaces.IAPIFacade;
import org.dse15.wait4op.apigw.session.SessionException;
import org.dse15.wait4op.apigw.session.UserSession;
import org.dse15.wait4op.apigw.util.Constants;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 */
@Path("/notifications")
@Produces(MediaType.APPLICATION_JSON)
public class NotificationResource extends BaseResource {

    public NotificationResource(IAPIFacade dataAggregator, UserSession userSession) {
        super(dataAggregator, userSession);
    }

    @GET
    public Response notifications(@QueryParam("since")String since,
                                  @CookieParam(Constants.AUTH_COOKIE_KEY) String authCookie) {

        try {

            String notifications = getAPIFacade().getNotifications(getUserSession().getUser(authCookie), since);
            return Response.ok(notifications).build();

        } catch (SessionException e) {
            return toResponse(new WebApplicationException(e.getMessage(), e, Response.Status.FORBIDDEN));
        } catch (Exception e) {
            e.printStackTrace();
            return toResponse(new WebApplicationException(e.getMessage(), e, Response.Status.INTERNAL_SERVER_ERROR));
        }

    }
}
