package org.dse15.wait4op.apigw.hystrix;

import com.netflix.hystrix.*;

/**
 *
 */
public class HystrixCommandDelegate<T> extends HystrixCommand<T> {

    private IHystrixDelegate<T> _delegate;

    public HystrixCommandDelegate(String name, IHystrixDelegate<T> delegate, int timeout) {
        super(Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey(name))
                        .andCommandKey(HystrixCommandKey.Factory.asKey(name))
                        .andThreadPoolKey(HystrixThreadPoolKey.Factory.asKey(name + "-ThreadPool"))
                        .andCommandPropertiesDefaults(HystrixCommandProperties.Setter()
                                .withExecutionTimeoutInMilliseconds(timeout)
                                .withExecutionIsolationStrategy(HystrixCommandProperties.ExecutionIsolationStrategy.THREAD)
                                .withCircuitBreakerRequestVolumeThreshold(3))
//                        .andThreadPoolPropertiesDefaults(HystrixThreadPoolProperties.Setter()
        );
        _delegate = delegate;
    }

    @Override
    protected T run() throws Exception {
        return _delegate.run();
    }
}