package org.dse15.wait4op.apigw.core;

import org.apache.http.client.HttpClient;
import org.dse15.wait4op.apigw.core.interfaces.IAPIFacade;
import org.dse15.wait4op.apigw.core.interfaces.IClient;

/**
 *
 */
public final class CoreFactory {

    private CoreFactory() {}

    public static IClient createRemoteClient(HttpClient client) {
        return new RemoteClient(client);
    }

    public static IClient createTestClient() {
        return new TestClient();
    }

    public static IAPIFacade createAPIFacade() {
        return new APIFacade();
    }
}