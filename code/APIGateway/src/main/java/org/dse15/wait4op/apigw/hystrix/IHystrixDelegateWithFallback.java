package org.dse15.wait4op.apigw.hystrix;

/**
 *
 */
public interface IHystrixDelegateWithFallback<T> extends IHystrixDelegate<T> {
    T getFallback();
}
