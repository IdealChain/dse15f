package org.dse15.wait4op.apigw.resources;

import org.dse15.wait4op.apigw.core.interfaces.IAPIFacade;
import org.dse15.wait4op.apigw.model.frontend.ExceptionWrapper;
import org.dse15.wait4op.apigw.session.UserSession;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

/**
 *
 */
abstract class BaseResource {

    private UserSession _userSession;
    private IAPIFacade _apiFacade;

    public BaseResource(IAPIFacade apiFacade, UserSession userSession) {
        _apiFacade = apiFacade;
        _userSession = userSession;
    }

    protected UserSession getUserSession() {
        return _userSession;
    }

    public IAPIFacade getAPIFacade() {
        return _apiFacade;
    }

    public Response toResponse(WebApplicationException e) {
        int status = e.getResponse().getStatus();
        String msg = e.getMessage();
        if (msg == null || msg.isEmpty())
            msg = e.getCause() != null ? e.getCause().getMessage() : "Empty WebApplicationException";
        return Response.status(status).entity(new ExceptionWrapper(status, msg)).build();
    }
}
