package org.dse15.wait4op.apigw.resources;

import org.dse15.wait4op.apigw.core.interfaces.IAPIFacade;
import org.dse15.wait4op.apigw.model.frontend.Credentials;
import org.dse15.wait4op.apigw.model.User;
import org.dse15.wait4op.apigw.session.SessionNotFoundException;
import org.dse15.wait4op.apigw.session.UserSession;
import org.dse15.wait4op.apigw.util.Constants;
import org.jvnet.hk2.annotations.Optional;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

/**
 *
 */
@Path("/auth")
@Produces(MediaType.APPLICATION_JSON)
public class AuthResource extends BaseResource {

    public AuthResource(IAPIFacade APIFacade, UserSession userSession) {
        super(APIFacade, userSession);
    }

    @GET
    @Path("/login")
    public Response login(@Optional @CookieParam(Constants.AUTH_COOKIE_KEY) String authCookie) {
        try {
            return Response.ok(getUserSession().getUser(authCookie)).build();
        } catch (SessionNotFoundException e) {
            return Response.ok().build();
        } catch (Exception e) {
            e.printStackTrace();
            return toResponse(new WebApplicationException(e.getMessage(), e, Response.Status.INTERNAL_SERVER_ERROR));
        }
    }

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(Credentials cred) {
        try {
            if (cred == null)
                return toResponse(new WebApplicationException("No credentials received", Response.Status.FORBIDDEN));

            User user = getAPIFacade().getUser(cred.getUsername(), cred.getPassword());

            if (user == null)
                return toResponse(new WebApplicationException("Invalid credentials received", Response.Status.FORBIDDEN));

            String sessionId = getUserSession().login(user);
            return Response.ok().cookie(new NewCookie(Constants.AUTH_COOKIE_KEY, sessionId, "/", null, null, -1, false)).entity(user).build();

        } catch (Exception e) {
            e.printStackTrace();
            return toResponse(new WebApplicationException(e.getMessage(), e, Response.Status.INTERNAL_SERVER_ERROR));
        }
    }

    @POST
    @Path("/logout")
    public Response logout(@Optional @CookieParam(Constants.AUTH_COOKIE_KEY) String authCookie) {
        try {
            getUserSession().logout(authCookie);
            return Response.ok().build();
        } catch (Exception e) {
            e.printStackTrace();
            return toResponse(new WebApplicationException(e.getMessage(), e, Response.Status.INTERNAL_SERVER_ERROR));
        }
    }
}