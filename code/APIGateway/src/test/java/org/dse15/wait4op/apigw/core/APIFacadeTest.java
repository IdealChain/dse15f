package org.dse15.wait4op.apigw.core;

import org.dse15.wait4op.apigw.core.exception.RemoteMicroServiceException;
import org.dse15.wait4op.apigw.core.interfaces.IAPIFacade;
import org.dse15.wait4op.apigw.core.interfaces.IClient;
import org.dse15.wait4op.apigw.model.User;
import org.dse15.wait4op.apigw.session.InsufficientAuthorizationException;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.URL;
import java.util.HashMap;
import java.util.List;

/**
 *
 */
public class APIFacadeTest {

    private static IAPIFacade _apiFacade;
    private static IClient _emptyArrayClient = new IClient() {
        @Override
        public String getResponse(String url, String content, String method) throws RemoteMicroServiceException {
            return "[]";
        }
    };
    private static IClient _exceptionClient = new IClient() {
        @Override
        public String getResponse(String url, String content, String method) throws RemoteMicroServiceException {
            throw new RemoteMicroServiceException("");
        }
    };

    @BeforeClass
    public static void init() throws Exception {
        _apiFacade = CoreFactory.createAPIFacade();

        URL dummyUrl = new URL("http://localhost:12345");

        HashMap<String, URL> ms = new HashMap<>();
        ms.put(APIFacade.MS_KLINISYS, dummyUrl);
        ms.put(APIFacade.MS_RESERVATION, dummyUrl);
        ms.put(APIFacade.MS_OPMATCHER, dummyUrl);
        ms.put(APIFacade.MS_NOTIFIER, dummyUrl);
        _apiFacade.registerMicroservices(ms);
    }

    /**
     * Tests if the APIFacade.getPatients(...) method will throw an exception,
     * if a wrong user-type requests it.
     *
     * @throws Exception
     */
    @Test(expected = InsufficientAuthorizationException.class)
    public void testGetPatientsIllegal() throws Exception {
        _apiFacade.registerClient(_emptyArrayClient);

        _apiFacade.getPatients(new User());
    }

    /**
     * Tests if the APIFacade.getPatients(...) method returns a valid (empty) list-object
     * of users.
     *
     * @throws Exception
     */
    @Test
    public void testGetPatientsAsDoctor() throws Exception {
        _apiFacade.registerClient(_emptyArrayClient);

        List<User> result = _apiFacade.getPatients(new User() {{ setUserType(UserType.DOCTOR);}});
        Assert.assertTrue(result.isEmpty());
    }

    /**
     * Tests if the APIFacade.getPatients(...) method returns a fallback list (empty list), in case
     * the IClient throws an exception (see _exceptionClient)
     *
     * @throws Exception
     */
    @Test()
    public void testGetUsersFallback() throws Exception {
        _apiFacade.registerClient(_exceptionClient);

        List<User> fallback = ((APIFacade)_apiFacade).getUsers();
        Assert.assertTrue(fallback.isEmpty());
    }
}
