package org.dse15.wait4op.apigw.session;

import org.dse15.wait4op.apigw.model.User;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 */
public class UserSessionTest {

    private static List<User> _users;

    @BeforeClass
    public static void init() {
        _users = new ArrayList<>();
        _users.add(new User() {{ setId("1"); setFullName("F T"); }});
        _users.add(new User() {{ setId("2"); setFullName("D A"); }});
        _users.add(new User() {{ setId("3"); setFullName("S W"); }});
        _users.add(new User() {{ setId("4"); setFullName("JWR"); }});
    }

    /**
     * Tests if the login works properly, by login + check if the user is still logged in
     */
    @Test
    public void testlogin() {
        UserSession us = new UserSession();
        User u1 = _users.get(0);
        User u2 = _users.get(1);

        String s1 = us.login(u1);
        String s2 = us.login(u2);

        Assert.assertTrue("User " + u1.getFullName() + " is not logged in", us.isLoggedIn(s1));
        Assert.assertTrue("User " + u2.getFullName() + " is not logged in", us.isLoggedIn(s2));

        Assert.assertFalse("Empty-String-Session must not be 'logged in'", us.isLoggedIn(""));
        Assert.assertFalse("Random-UUID-String-Session must not be 'logged in'", us.isLoggedIn(UUID.randomUUID().toString()));
    }

    /**
     * Tests if the user-session does properly return the same user object after it was logged in beforehand.
     * @throws SessionException
     */
    @Test
    public void testGetUser() throws SessionException {
        UserSession us = new UserSession();
        User u3 = _users.get(2);
        User u4 = _users.get(3);

        String s3 = us.login(u3);
        String s4 = us.login(u4);

        User retrievedU3 = us.getUser(s3);
        User retrievedU4 = us.getUser(s4);

        Assert.assertEquals("Did not retrieve correct user.", u3, retrievedU3);
        Assert.assertEquals("Did not retrieve correct user.", u4, retrievedU4);
    }
}