package org.dse15.wait4op.reser.resource;

import org.dse15.wait4op.reser.helper.JsonToObject;
import org.dse15.wait4op.reser.model.Clinic;
import org.dse15.wait4op.reser.model.Slot;
import org.dse15.wait4op.reser.model.User;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 */
public class TestSlotResource {

    @Test
    public void testJsonToUser() throws Exception {

        User u = JsonToObject.jsonToUser("{\"id\":\"1\", \"firstName\": \"F\", \"lastName\": \"L\"}");

        Assert.assertEquals(u.getUserId(), "1");
        Assert.assertEquals(u.getFirstName(), "F");
        Assert.assertEquals(u.getLastName(), "L");
    }

    @Test
    public void testJsonToClinic() throws Exception {

        Clinic c = JsonToObject.jsonToClinic("{\"id\":\"1\", \"name\": \"C\"}");

        Assert.assertEquals("1", c.getClinicId());
        Assert.assertEquals("C", c.getName());
    }

    @Test
    public void testJsonToSlot() throws Exception {

        Slot s = JsonToObject.jsonToSlot("{\"id\":\"1\", \"from\": 1, \"to\": 2, \"hospitalId\": \"h1\"}");

        Assert.assertEquals("1", s.getId());
        Assert.assertEquals(new Long(1l), s.getFrom());
        Assert.assertEquals(new Long(2l), s.getTo());
        Assert.assertEquals("h1", s.getHospitalId());
    }
}
