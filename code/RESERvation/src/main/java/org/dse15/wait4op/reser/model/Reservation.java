package org.dse15.wait4op.reser.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Reservation {
    private String _slotId;
    private String _patientId;
    private String _doctorId;

    @JsonProperty("slotId")
    public String getSlotId() {
        return _slotId;
    }

    public void setSlotId(String slotId) {
        _slotId = slotId;
    }

    @JsonProperty("patientId")
    public String getPatientId() {
        return _patientId;
    }

    public void setPatientId(String patientId) {
        _patientId = patientId;
    }

    @JsonProperty("doctorId")
    public String getDoctorId() {
        return _doctorId;
    }

    public void setDoctorId(String doctorId) {
        _doctorId = doctorId;
    }
}
