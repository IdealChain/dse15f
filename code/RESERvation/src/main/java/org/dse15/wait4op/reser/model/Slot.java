package org.dse15.wait4op.reser.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Slot {

    private String _id;
    private Long _from;
    private Long _to;
    private String _hospitalId;

    @JsonProperty("id")
    public String getId() {
        return _id;
    }

    public void setId(String id) {
        _id = id;
    }

    @JsonProperty("from")
    public Long getFrom() {
        return _from;
    }

    public void setFrom(Long from) {
        _from = from;
    }

    @JsonProperty("to")
    public Long getTo() {
        return _to;
    }

    public void setTo(Long to) {
        _to = to;
    }

    @JsonProperty("hospitalId")
    public String getHospitalId() {
        return _hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        _hospitalId = hospitalId;
    }
}