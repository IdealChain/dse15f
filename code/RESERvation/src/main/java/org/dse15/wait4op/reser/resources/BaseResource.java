package org.dse15.wait4op.reser.resources;

import org.dse15.wait4op.reser.core.IPersistenceFacade;
import org.dse15.wait4op.reser.core.RemoteClient;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
abstract class BaseResource {

    private IPersistenceFacade _facade;
    private List<String> _blockedSlotIds;
    private RemoteClient _remoteClient;
    private Map<String, URL> _microservices;

    public BaseResource(IPersistenceFacade facade) {
        this(facade, null, new HashMap<>());
    }

    public BaseResource(IPersistenceFacade facade, RemoteClient remoteClient, Map<String, URL> microservices) {
        _facade = facade;
        _blockedSlotIds = new ArrayList<>();
        _remoteClient = remoteClient;
        _microservices = microservices;
    }

    protected IPersistenceFacade getPersistenceFacade() {
        return _facade;
    }

    protected RemoteClient getRemoteClient() {
        return _remoteClient;
    }

    protected boolean canReserve(String slotId) {
        return !_blockedSlotIds.contains(slotId);
    }

    protected void block(String slotId) {
        _blockedSlotIds.add(slotId);
    }

    protected void release(String slotId) {
        _blockedSlotIds.remove(slotId);
    }

    protected Map<String, URL> getMicroservices() {
        return _microservices;
    }
}
