package org.dse15.wait4op.reser;

import io.dropwizard.Application;
import io.dropwizard.client.HttpClientBuilder;
import io.dropwizard.setup.Environment;
import org.dse15.wait4op.reser.core.*;
import org.dse15.wait4op.reser.resources.ReservationResource;
import org.dse15.wait4op.reser.resources.SlotResource;

/**
 *
 */
public class RESERvationApplication extends Application<RESERvationConfiguration> {

    public static void main(String[] args) throws Exception {
        new RESERvationApplication().run(args);
    }

    @Override
    public void run(RESERvationConfiguration config, Environment environment) throws Exception {

        RemoteClient rc = new RemoteClient(new HttpClientBuilder(environment).using(config.getHttpClientConfiguration()).build("Client"));

        IDBFacade dbFacade = new MongoDBFacade(config.getMongohost(), config.getMongoport(), config.getMongodb());
        IPersistenceFacade pFacade = new PersistenceFacade(dbFacade);

        environment.jersey().register(new ReservationResource(pFacade, rc, config.getMicroservices()));
        environment.jersey().register(new SlotResource(pFacade, rc, config.getMicroservices()));
    }
}
