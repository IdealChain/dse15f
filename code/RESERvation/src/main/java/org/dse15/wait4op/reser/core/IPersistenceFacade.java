package org.dse15.wait4op.reser.core;

import org.dse15.wait4op.reser.core.exception.MicroServiceException;
import org.dse15.wait4op.reser.model.Reservation;

import java.util.List;

/**
 *
 */
public interface IPersistenceFacade {

    List<Reservation> getReservations(List<String> slotIds) throws MicroServiceException;

    void createReservation(Reservation reservation) throws MicroServiceException;

    void deleteReservation(String reservationId) throws MicroServiceException;
}
