package org.dse15.wait4op.reser.core;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.IndexOptions;
import org.bson.BsonDocument;
import org.bson.Document;
import org.dse15.wait4op.reser.core.exception.MicroServiceException;
import org.dse15.wait4op.reser.core.exception.ReservationAlreadyExistsException;
import org.dse15.wait4op.reser.model.Reservation;

import javax.print.Doc;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 */
public class MongoDBFacade implements IDBFacade {

    private static final String COLLECTION_NAME = "reservations";

    private MongoClient _mongoClient;
    private MongoDatabase _mongoDatabase;
    private MongoCollection<Document> _mongoCollection;

    private ReentrantLock _lock = new ReentrantLock();

    public MongoDBFacade(String host, int port, String db) {
        _mongoClient = new MongoClient(host, port);
        _mongoDatabase = _mongoClient.getDatabase(db);
        _mongoCollection = _mongoDatabase.getCollection(COLLECTION_NAME);
        _mongoCollection.createIndex(new BasicDBObject("slotId", 1), new IndexOptions(){{ unique(true); }});
    }

    @Override
    public synchronized void createNew(Reservation reservation) throws MicroServiceException {

        _lock.lock();
        try {
            List<Reservation> existingReservation = findReservations(new ArrayList<String>(1) {{
                add(reservation.getSlotId());
            }});
            if (!existingReservation.isEmpty())
                throw new ReservationAlreadyExistsException(String.format("A reservation already exists for slotId=%s", reservation.getSlotId()));

            ObjectMapper mapper = new ObjectMapper();
            String reservationJson = mapper.writeValueAsString(reservation);
            Document reservationDoc = Document.parse(reservationJson);
            _mongoCollection.insertOne(reservationDoc);

        } catch(JsonProcessingException e) {
            throw new MicroServiceException("Could not convert JSON into String", e);
        } finally {
            _lock.unlock();
        }
    }

    @Override
    public synchronized List<Reservation> findReservations(List<String> slotIds) throws MicroServiceException {
        FindIterable<Document> iterable = _mongoCollection.find(Filters.in("slotId", slotIds));

        List<Reservation> result = new ArrayList<>(slotIds.size());
        try {
            JsonFactory factory = new JsonFactory();
            ObjectMapper mapper = new ObjectMapper();
            for (Document document : iterable) {
                JsonParser parser = factory.createParser(document.toJson());
                result.add(mapper.readValue(parser, Reservation.class));
            }
            return result;
        } catch (IOException e) {
            throw new MicroServiceException("Error while deserializing JSON to Reservation-Object", e);
        }
    }

    @Override
    public synchronized void deleteReservation(String id) throws MicroServiceException {

        _lock.lock();
        try {

            _mongoCollection.deleteOne(Filters.eq("slotId", id));

        } finally {
            _lock.unlock();
        }
    }
}
