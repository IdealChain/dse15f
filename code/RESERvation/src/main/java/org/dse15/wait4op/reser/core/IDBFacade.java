package org.dse15.wait4op.reser.core;

import org.dse15.wait4op.reser.core.exception.MicroServiceException;
import org.dse15.wait4op.reser.core.exception.NotFoundException;
import org.dse15.wait4op.reser.core.exception.ReservationAlreadyExistsException;
import org.dse15.wait4op.reser.model.Reservation;

import java.util.List;

/**
 *
 */
public interface IDBFacade {

    void createNew(Reservation reservation) throws MicroServiceException, ReservationAlreadyExistsException;

    List<Reservation> findReservations(List<String> slotIds) throws MicroServiceException;

    void deleteReservation(String id) throws MicroServiceException, NotFoundException;
}
