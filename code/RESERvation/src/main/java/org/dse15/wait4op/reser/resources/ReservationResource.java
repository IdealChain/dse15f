package org.dse15.wait4op.reser.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.dse15.wait4op.reser.core.IPersistenceFacade;
import org.dse15.wait4op.reser.core.RemoteClient;
import org.dse15.wait4op.reser.helper.JsonToObject;
import org.dse15.wait4op.reser.model.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Path("/reservation")
@Produces(MediaType.APPLICATION_JSON)
public class ReservationResource extends BaseResource {

    private static final String MS_NOTIFIER = "notifier";
    private static final String MS_KLINISYS = "klinisys";

    public ReservationResource(IPersistenceFacade facade, RemoteClient remoteClient, Map<String, URL> microservices) {
        super(facade, remoteClient, microservices);
    }

    @GET
    public Response getReservations(@QueryParam("ids") List<String> ids) {
        try {

            if (ids == null || ids.isEmpty())
                return Response.ok("{ }").build();

            List<Reservation> reservations = getPersistenceFacade().getReservations(ids);
            return Response.ok(reservations).build();

        } catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(e.getMessage(), e, Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createReservation(Reservation reservation) {
        try {
            if (!canReserve(reservation.getSlotId()))
                throw new WebApplicationException("SlotId=" + reservation.getSlotId() + " is being deleted. Cannot create reservation");

            getPersistenceFacade().createReservation(reservation);
            return Response.ok().build();

        } catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(e.getMessage(), e, Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    @DELETE
    @Path("/{id}")
    public Response deleteReservation(@PathParam("id") String id) {
        try {
            List<Reservation> reservations = getPersistenceFacade().getReservations(new ArrayList<String>() {{
                add(id);
            }});

            if (reservations.isEmpty())
                return Response.ok().build();

            getPersistenceFacade().deleteReservation(id);

            try {
                notifyUsers(reservations.get(0));
            } catch (Exception e) {
                e.printStackTrace();
            }

            return Response.ok().build();

        } catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(e.getMessage(), e, Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    private void notifyUsers(Reservation delRes) throws Exception {

        String slotJsonString = getRemoteClient().getResponse(
                String.format("%s/slots/%s", getMicroservices().get(MS_KLINISYS).toExternalForm(), delRes.getSlotId()), null, HttpMethod.GET);
        Slot slot = JsonToObject.jsonToSlot(slotJsonString);

        String patientJsonString = getRemoteClient().getResponse(
                String.format("%s/masterdata/%s", getMicroservices().get(MS_KLINISYS).toExternalForm(), delRes.getPatientId()), null, HttpMethod.GET);
        User patient = JsonToObject.jsonToUser(patientJsonString);

        String doctorJsonString = getRemoteClient().getResponse(
                String.format("%s/masterdata/%s", getMicroservices().get(MS_KLINISYS).toExternalForm(), delRes.getDoctorId()), null, HttpMethod.GET);
        User doctor = JsonToObject.jsonToUser(doctorJsonString);

        String clinicJsonString = getRemoteClient().getResponse(
                String.format("%s/masterdata/%s", getMicroservices().get(MS_KLINISYS).toExternalForm(), slot.getHospitalId()), null, HttpMethod.GET);
        Clinic clinic = JsonToObject.jsonToClinic(clinicJsonString);

        //
        // notifiy all relevant users
        //
        ObjectMapper mapper = new ObjectMapper();

        Notification patientNotification = new Notification();
        patientNotification.setUserId(delRes.getPatientId());
        Context patientContext = new Context();
        patientContext.setDoctor(doctor);        //TODO
        patientContext.setClinic(clinic);        //TODO
        patientContext.setSlot(slot);        //TODO
        patientNotification.setContext(patientContext);
        patientNotification.setMessage("Reservierung storniert!");

        Notification doctorNotification = new Notification();
        doctorNotification.setUserId(delRes.getDoctorId());
        Context doctorContext = new Context();
        doctorContext.setPatient(patient);        //TODO
        doctorContext.setClinic(clinic);        //TODO
        doctorContext.setSlot(slot);        //TODO
        doctorNotification.setContext(doctorContext);
        doctorNotification.setMessage("Reservierung storniert!");

        Notification clinicNotification = new Notification();
        clinicNotification.setUserId(slot.getHospitalId());        //TODO
        Context clinicContext = new Context();
        clinicContext.setPatient(patient);        //TODO
        clinicContext.setDoctor(doctor);        //TODO
        clinicContext.setSlot(slot);        //TODO
        clinicNotification.setContext(clinicContext);
        clinicNotification.setMessage("Reservierung storniert!");

        try {
            getRemoteClient().getResponse(
                    String.format("%s/notifications", getMicroservices().get(MS_NOTIFIER).toExternalForm()),
                    mapper.writeValueAsString(patientNotification), HttpMethod.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            getRemoteClient().getResponse(
                    String.format("%s/notifications", getMicroservices().get(MS_NOTIFIER).toExternalForm()),
                    mapper.writeValueAsString(doctorNotification), HttpMethod.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            getRemoteClient().getResponse(
                    String.format("%s/notifications", getMicroservices().get(MS_NOTIFIER).toExternalForm()),
                    mapper.writeValueAsString(clinicNotification), HttpMethod.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
