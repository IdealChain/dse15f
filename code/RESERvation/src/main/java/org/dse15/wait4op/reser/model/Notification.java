package org.dse15.wait4op.reser.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Notification {

    private String _message;
    private String _userId;
    private Context _context;


    @JsonProperty("message")
    public String getMessage() {
        return _message;
    }

    public void setMessage(String message) {
        _message = message;
    }

    @JsonProperty("userId")
    public String getUserId() {
        return _userId;
    }

    public void setUserId(String userId) {
        _userId = userId;
    }

    @JsonProperty("context")
    public Context getContext() {
        return _context;
    }

    public void setContext(Context context) {
        _context = context;
    }
}
