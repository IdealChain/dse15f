package org.dse15.wait4op.reser.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Clinic {

    private String _clinicId;
    private String _name;

    @JsonProperty("id")
    public String getClinicId() {
        return _clinicId;
    }

    public void setClinicId(String clinicId) {
        _clinicId = clinicId;
    }

    @JsonProperty("name")
    public String getName() {
        return _name;
    }

    public void setName(String name) {
        _name = name;
    }
}
