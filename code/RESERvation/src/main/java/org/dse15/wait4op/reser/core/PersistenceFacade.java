package org.dse15.wait4op.reser.core;

import org.dse15.wait4op.reser.core.exception.MicroServiceException;
import org.dse15.wait4op.reser.model.Reservation;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 */
public class PersistenceFacade implements IPersistenceFacade {

    private IDBFacade _dbFacade;

    public PersistenceFacade(IDBFacade dbFacade) throws Exception {
        _dbFacade = dbFacade;
    }

    @Override
    public List<Reservation> getReservations(List<String> slotIds) throws MicroServiceException {
        if (slotIds == null || slotIds.isEmpty())
            return Collections.<Reservation>emptyList();
        return _dbFacade.findReservations(slotIds);
    }

    @Override
    public void createReservation(Reservation reservation) throws MicroServiceException {
        if (isInvalid(reservation))
            throw new MicroServiceException("Cannot create reservation which is null, or has null properties.");

        _dbFacade.createNew(reservation);
    }

    @Override
    public void deleteReservation(String reservationId) throws MicroServiceException {
        _dbFacade.deleteReservation(reservationId);
    }

    private boolean isInvalid(Reservation r) {
        return (r == null || r.getSlotId() == null || r.getDoctorId() == null || r.getPatientId() == null);
    }
}
