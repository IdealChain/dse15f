package org.dse15.wait4op.reser.helper;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.dse15.wait4op.reser.model.Clinic;
import org.dse15.wait4op.reser.model.Slot;
import org.dse15.wait4op.reser.model.User;

/**
 *
 */
public class JsonToObject {

    public static Slot jsonToSlot(String jsonString) throws Exception {

        Slot result = new ObjectMapper().readValue(jsonString, Slot.class);
        return result;
    }

    public static User jsonToUser(String userJsonString) throws Exception {

        JsonNode jsonUser = new ObjectMapper().readTree(userJsonString);

        User user = new User();
        user.setUserId(jsonUser.get("id").asText());
        user.setFirstName(jsonUser.get("firstName").asText());
        user.setLastName(jsonUser.get("lastName").asText());
        if(jsonUser.has("title"))
            user.setTitle(jsonUser.get("title").asText());

        return user;
    }

    public static Clinic jsonToClinic(String clinicJsonString) throws Exception {

        JsonNode jsonClinic = new ObjectMapper().readTree(clinicJsonString);

        Clinic clinic = new Clinic();
        clinic.setClinicId(jsonClinic.get("id").asText());
        clinic.setName(jsonClinic.get("name").asText());

        return clinic;
    }
}
