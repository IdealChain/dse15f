package org.dse15.wait4op.reser.resources;

import org.dse15.wait4op.reser.core.IPersistenceFacade;
import org.dse15.wait4op.reser.core.RemoteClient;
import org.dse15.wait4op.reser.model.Reservation;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Path("/slots")
@Produces(MediaType.APPLICATION_JSON)
public class SlotResource extends BaseResource {

    private static final String MS_KLINISYS = "klinisys";

    public SlotResource(IPersistenceFacade facade, RemoteClient remoteClient, Map<String, URL> microservices) {
        super(facade, remoteClient, microservices);
    }

    @DELETE
    @Path("/{id}")
    public Response deleteSlot(@PathParam("id") String slotId) {
        try {
            block(slotId);

            List<Reservation> reservations = getPersistenceFacade().getReservations(new ArrayList<String>() {{
                add(slotId);
            }});
            if (!reservations.isEmpty()) {
                throw new WebApplicationException("Cannot delete slotId=" + slotId + ", because a reservation already exists");
            }

            getRemoteClient().getResponse(
                    String.format("%s/slots/%s", getMicroservices().get(MS_KLINISYS).toExternalForm(), slotId), null, HttpMethod.DELETE);

            return Response.ok().build();
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(e.getMessage(), e, Response.Status.INTERNAL_SERVER_ERROR);
        } finally {
            release(slotId);
        }
    }
}
