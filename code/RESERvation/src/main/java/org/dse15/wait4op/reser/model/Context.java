package org.dse15.wait4op.reser.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Context {

    private User _patient;
    private User _doctor;
    private Slot _slot;
    private Clinic _clinic;

    @JsonProperty("patient")
    public User getPatient() {
        return _patient;
    }

    public void setPatient(User patient) {
        _patient = patient;
    }

    @JsonProperty("doctor")
    public User getDoctor() {
        return _doctor;
    }

    public void setDoctor(User doctor) {
        _doctor = doctor;
    }

    @JsonProperty("slot")
    public Slot getSlot() {
        return _slot;
    }

    public void setSlot(Slot slot) {
        _slot = slot;
    }

    @JsonProperty("clinic")
    public Clinic getClinic() {
        return _clinic;
    }

    public void setClinic(Clinic clinic) {
        _clinic = clinic;
    }
}