package org.dse15.wait4op.reser;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.client.HttpClientConfiguration;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.net.URL;
import java.util.Collections;
import java.util.Map;


@JsonIgnoreProperties(ignoreUnknown = true)
public class RESERvationConfiguration extends Configuration {

    @NotNull
    private Map<String, URL> _microservices = Collections.emptyMap();

    @Valid
    @NotNull
    private HttpClientConfiguration _httpClientConfiguration = new HttpClientConfiguration();

    private String _mongohost;
    private int _mongoport;
    private String _mongodb;

    @JsonProperty
    @NotEmpty
    public String getMongohost() {
        return _mongohost;
    }

    public void setMongohost(String mongohost) {
        _mongohost = mongohost;
    }

    @JsonProperty
    @Min(1)
    @Max(65535)
    public int getMongoport() {
        return _mongoport;
    }

    public void setMongoport(int mongoport) {
        _mongoport = mongoport;
    }

    @JsonProperty
    @NotEmpty
    public String getMongodb() {
        return _mongodb;
    }

    public void setMongodb(String mongodb) {
        _mongodb = mongodb;
    }

    @JsonProperty("microservices")
    public Map<String, URL> getMicroservices() {
        return _microservices;
    }

    @JsonProperty("microservices")
    public void setMicroservices(Map<String, URL> microservices) {
        _microservices = microservices;
    }

    @JsonProperty("httpClient")
    public HttpClientConfiguration getHttpClientConfiguration() {
        return _httpClientConfiguration;
    }

    @JsonProperty("httpClient")
    public void setHttpClientConfiguration(HttpClientConfiguration hcc) {
        _httpClientConfiguration = hcc;
    }
}