package wait4op

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import com.typesafe.config._

object View {

  val public = exec(http("Home")
      .get("/"))
    .repeat(10) {
      pause(5)
        .exec(http("PublicOPs")
          .get("/api/publicOPSlots"))
    }

  val clinic =
    repeat(10) {
      pause(5)
        .exec(http("PublicOPs")
          .get("/api/publicOPSlots"))
        .exec(http("MyOPs [Clinic]")
          .get("/api/myOPSlots"))
    }

  val doctor =
    repeat(10) {
      pause(5)
        .exec(http("PublicOPs")
        .get("/api/publicOPSlots"))
        .exec(http("MyOPs [Doctor]")
        .get("/api/myOPSlots"))
    }

  val patient =
    repeat(10) {
      pause(5)
        .exec(http("PublicOPs")
        .get("/api/publicOPSlots"))
        .exec(http("MyOPs [Patient]")
        .get("/api/myOPSlots"))
    }
}

object Login {
  val clinic_feeder = csv("clinic_credentials.csv").random
  val doctor_feeder = csv("doctor_credentials.csv").random
  val patient_feeder = csv("patient_credentials.csv").random

  val clinic =
    exec(http("Home")
        .get("/"))
      .pause(3)
      .feed(clinic_feeder)
      .exec(http("Login [Clinic]")
        .post("/api/auth/login")
        .body(StringBody("""{ "username": "${username}", "password": "${password}" }""")).asJSON
      )
      .exec(http("RoleView")
        .get("/views/clinic.html"))

  val doctor =
    exec(http("Home")
      .get("/"))
    .pause(3)
    .feed(doctor_feeder)
    .exec(http("Login [Doctor]")
      .post("/api/auth/login")
      .body(StringBody("""{ "username": "${username}", "password": "${password}" }""")).asJSON
    )
    .exec(http("RoleView")
      .get("/views/doctor.html"))

  val patient =
    exec(http("Home")
        .get("/"))
      .pause(3)
      .feed(patient_feeder)
      .exec(http("Login [Patient]")
        .post("/api/auth/login")
        .body(StringBody("""{ "username": "${username}", "password": "${password}" }""")).asJSON
      )
    .exec(http("RoleView")
      .get("/views/patient.html"))
}

class BasicSimulation extends Simulation {

  val conf = ConfigFactory.load("application.conf")
  val baseUrl = conf.getString("http.url")

  val httpProtocol = http
    .baseURL(baseUrl) // Here is the root for all relative URLs
    .inferHtmlResources()
    .acceptHeader("application/json, text/plain, */*")
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .connection("keep-alive")
    .contentTypeHeader("application/json;charset=utf-8")
    .userAgentHeader("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0")

  val public = scenario("Public Users").exec(View.public)
  val clinics = scenario("Clinic Users").exec(Login.clinic, View.clinic)
  val doctors = scenario("Doctor Users").exec(Login.doctor, View.doctor)
  val patients = scenario("Patient Users").exec(Login.patient, View.patient)

  setUp(
    public.inject(rampUsers(50) over (20 seconds)),
    clinics.inject(rampUsers(10) over (10 seconds)),
    doctors.inject(rampUsers(15) over (10 seconds)),
    patients.inject(rampUsers(30) over (20 seconds))
  ).protocols(httpProtocol)
}
