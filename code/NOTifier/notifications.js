"use strict";

var chai = require('chai'), expect = chai.expect;
chai.use(require('chai2-json-schema'));

var notifications = [];

var notificationSchema = {
    title: 'notification',
    type: 'object',
    required: ['userId', 'message'],
    properties: {
        userId: {
            type: 'string',
            minLength: 1
        },
        message: {
            type: 'string',
            minLength: 1
        },
        isError: {type: 'boolean'}
    }
};

module.exports.store = function (notification) {

    expect(notification).to.be.jsonSchema(notificationSchema);

    var userId = notification.userId;
    notification.timestamp = Date.now();

    if (notifications[userId] === undefined)
        notifications[userId] = [];

    notifications[userId].push(notification);
};

module.exports.get = function (userId, since) {

    expect(userId, 'userId').to.be.a('string').and.to.have.length.of.at.least(1);
    if (since !== undefined)
        expect(since, 'since').to.be.a('number').and.be.above(0);

    if (notifications[userId] === undefined)
        return [];

    return notifications[userId].filter(function (n) {
        return since === undefined || n.timestamp >= since;
    });

};
