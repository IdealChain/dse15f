#!/usr/bin/env node
"use strict";

var bunyan = require('bunyan');
var nconf = require('nconf');
var restify = require('restify');
var notifications = require('./notifications');

// init configuration and logging
nconf.argv().env().file({file: './config.json'});
var log = bunyan.createLogger({name: 'NOTifier'});

// init restify, log requests
var server = restify.createServer({log: log});
server.use(restify.bodyParser({mapParams: false}));
server.use(restify.queryParser());
server.use(restify.requestLogger());

// default route: show bare routing endpoints
server.get('/', function (req, res, next) {
    var routes = Object.keys(server.router.mounts).map(function (key) {
        return server.router.mounts[key].spec;
    });

    res.send(routes);
    next();
});

// handle incoming notification post
server.post('/notifications', function (req, res, next) {

    notifications.store(req.body);
    log.info('Stored %j', req.body);

    res.send(200, {ok: true});
    next();
});

// handle incoming request for new notifications
server.get('/notifications/:userId?since=:since', function (req, res, next) {
    var userId = req.params.userId;
    var since;

    if (req.query.since !== undefined)
        since = parseInt(req.query.since);

    var result = {
        timestamp: Date.now(),
        notifications: notifications.get(userId, since)
    };

    if (result.notifications.length)
        log.info('Delivering %d notification(s) for user "%s"', result.notifications.length, userId);

    res.send(200, result);
    next();
});

server.listen(nconf.get('http:port'), function () {
    log.info('NOTifier ready at %s.', server.url);
});
