"use strict";

var expect = require('chai').expect;
var notifications = require('../notifications');

describe('notifications', function () {
    describe('store', function () {

        it('should store notification', function () {
            notifications.store({userId: "abc", message: "hello"});
        });

        it('should throw on invalid schema', function () {
            function storeEmpty() {
                notifications.store({});
            }

            function storeEmptyUser() {
                notifications.store({userId: '', message: 'hello'});
            }

            expect(storeEmpty).to.throw('Missing required property');
            expect(storeEmptyUser).to.throw('String is too short');
        });
    });

    describe('get', function () {

        it('should return user notification', function () {
            var n = {userId: "get1", message: "hello"};
            notifications.store(n);

            var results = notifications.get("get1");
            expect(results).to.have.length(1);
            expect(results[0]).to.deep.equal(n);
        });

        it('should return notification since', function (done) {
            var n1 = {userId: "get2", message: "1"};
            var n2 = {userId: "get2", message: "2"};

            notifications.store(n1);

            function storeSecondNotification() {
                var middle = Date.now();
                notifications.store(n2);

                expect(notifications.get("get2")).to.deep.equal([n1, n2]);
                expect(notifications.get("get2", middle)).to.deep.equal([n2]);
                done();
            }

            setTimeout(storeSecondNotification, 20);
        });
    });
});
