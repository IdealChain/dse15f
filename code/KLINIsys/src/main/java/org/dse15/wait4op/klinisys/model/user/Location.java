package org.dse15.wait4op.klinisys.model.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by sylvia on 30.05.15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Location {

    private String _type;
    // [<longitude>,<latitude>]
    private double[] _coordinates;

    @JsonProperty
    public String getType() {
        return _type;
    }

    public void setType(String type) {
        _type = type;
    }

    @JsonProperty
    public double[] getCoordinates() {
        return _coordinates;
    }

    public void setCoordinates(double[] coordinates) {
        _coordinates = coordinates;
    }

}
