package org.dse15.wait4op.klinisys.model.user;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by sylvia on 26.05.15.
 */


public class UserTemplate {

    public UserTemplate() {
    }

    private Type _type;
    private String _title;
    private String _firstName;
    private String _lastName;
    private String _loginName;
    private String _password;
    private Gender _gender;
    private String _hospitalName;
    private Location _location;
    @JsonProperty
    public Type getType() {
        return _type;
    }

    public void setType(Type type) {
        _type = type;
    }
    @JsonProperty
    public String getTitle() {
        return _title;
    }

    public void setTitle(String title) {
        _title = title;
    }
    @JsonProperty
    public String getFirstName() {
        return _firstName;
    }

    public void setFirstName(String firstName) {
        _firstName = firstName;
    }
    @JsonProperty
    public String getLastName() {
        return _lastName;
    }

    public void setLastName(String lastName) {
        _lastName = lastName;
    }
    @JsonProperty
    public String getLoginName() {
        return _loginName;
    }

    public void setLoginName(String loginName) {
        _loginName = loginName;
    }
    @JsonProperty
    public String getPassword() {
        return _password;
    }

    public void setPassword(String password) {
        _password = password;
    }
    @JsonProperty
    public Gender getGender() {
        return _gender;
    }

    public void setGender(Gender gender) {
        _gender = gender;
    }
    @JsonProperty
    public String getHospitalName() {
        return _hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        _hospitalName = hospitalName;
    }

    @JsonProperty
    public Location getLocation() {
        return _location;
    }

    public void setLocation(Location location) {
        _location = location;
    }
}