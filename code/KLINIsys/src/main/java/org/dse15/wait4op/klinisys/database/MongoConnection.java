package org.dse15.wait4op.klinisys.database;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;


/**
 * Created by sylvia on 17.05.15.
 */
public class MongoConnection {

    private static MongoConnection _connection;
    private static String _server = "localhost";
    private static int _port = 27017;
    private static String _database = "masterdata";

    private MongoClient _mongoClient;
    private String _userCollection = "user";
    private String _slotCollection = "slot";

    private MongoConnection() {
        _mongoClient = new MongoClient(_server, _port);
        createIndex();
    }

    public static void setupMongoConnection(String host, int port, String database) {
        _server = host;
        _port = port;
        _database = database;
    }

    public static MongoConnection getInstance() {
        if(_connection == null)
            _connection = new MongoConnection();

        return _connection;
    }

    public MongoDatabase getDatabase() {
        return _mongoClient.getDatabase(_database);
    }

    public MongoCollection<Document> getCollection(String database, String collection) {
        MongoDatabase db = _mongoClient.getDatabase(database);
        return db.getCollection(collection);
    }

    public MongoCollection<Document> getUserCollection() {
        MongoDatabase db = _mongoClient.getDatabase(_database);

        return db.getCollection(_userCollection);
    }

    public MongoCollection<Document> getSlotCollection() {
        MongoDatabase db = _mongoClient.getDatabase(_database);
        return db.getCollection(_slotCollection);
    }

    private void createIndex() {
        MongoDatabase db = _mongoClient.getDatabase(_database);

        MongoCollection<Document> collection = db.getCollection(_userCollection);
        collection.createIndex(new Document("location", "2dsphere"));
    }
}
