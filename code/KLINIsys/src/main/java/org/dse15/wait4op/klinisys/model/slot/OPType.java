package org.dse15.wait4op.klinisys.model.slot;

/**
 * Created by sylvia on 27.05.15.
 */
public enum OPType {
    Augenheilkunde, Orthopaedie, HNO, Neurochirurgie, Kardiologie;

    public static OPType parse(String type) {
        if (type == null)
            return null;

        switch (type.toLowerCase()) {
            case "augenheilkunde": return Augenheilkunde;
            case "orthopaedie": return Orthopaedie;
            case "hno": return HNO;
            case "neurochirurgie": return Neurochirurgie;
            case "kardiologie": return Kardiologie;
            default: return null;
        }
    }
}
