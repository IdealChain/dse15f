package org.dse15.wait4op.klinisys.model.user;

/**
 * Created by sylvia on 23.05.15.
 */
public enum Type {
    DOCTOR,
    PATIENT,
    HOSPITAL
}
