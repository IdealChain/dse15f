package org.dse15.wait4op.klinisys.resources;

import org.bson.types.ObjectId;
import org.dse15.wait4op.klinisys.database.DBFacade;
import org.dse15.wait4op.klinisys.helper.Helper;
import org.dse15.wait4op.klinisys.model.slot.OPType;
import org.dse15.wait4op.klinisys.model.slot.Slot;
import org.dse15.wait4op.klinisys.model.user.Id;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by sylvia on 26.05.15.
 */
@Path("/slots")
@Produces(MediaType.APPLICATION_JSON)
public class Slots {

    @GET
    public Response getSlots() throws Exception {

        DBFacade facade = DBFacade.getInstance();
        return Response.ok(Helper.objectToJson(facade.getAllSlots())).build();
    }

    @GET
    @Path("/{slotId}")
    public Response byId(@PathParam("slotId") String slotId) throws Exception {

        DBFacade facade = DBFacade.getInstance();
        Slot s = facade.getSlotById(new ObjectId(slotId));
        System.out.println(s.toString());
        return Response.ok(Helper.objectToJson(s)).build();
    }

    @GET
    @Path("/slotTypes")
    public Response slotTypes() throws Exception {
        return Response.ok(Helper.objectToJson(OPType.values())).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response insertSlot(Slot slot) throws Exception {

        DBFacade facade = DBFacade.getInstance();
        String id = facade.persistSlot(slot);
        return Response.ok(Helper.objectToJson(new Id(id))).build();

    }

    @DELETE
    @Path("/{slotId}")
    public Response delete(@PathParam("slotId") String slotId) {

        DBFacade facade = DBFacade.getInstance();
        facade.removeSlotById(new ObjectId(slotId));
        return Response.ok().build();
    }
}
