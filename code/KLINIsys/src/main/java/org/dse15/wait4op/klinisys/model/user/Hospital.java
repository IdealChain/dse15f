package org.dse15.wait4op.klinisys.model.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by sylvia on 16.05.15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Hospital extends User {

    private String _name;
    private Location _location;

    @JsonProperty
    public String getName() {
        return _name;
    }

    public void setName(String name) {
        _name = name;
    }

    @Override
    public String toString() {
        return _name + ": " + super.toString();
    }

    @JsonProperty
    public Location getLocation() {
        return _location;
    }

    public void setLocation(Location location) {
        _location = location;
    }
}
