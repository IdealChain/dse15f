package org.dse15.wait4op.klinisys.model.user;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by sylvia on 26.05.15.
 */
public class Id {
    private String _id;

    public Id(String id) {
        _id = id;
    }

    @JsonProperty
    public String getId() {
        return _id;
    }

    public void setId(String id) {
        _id = id;
    }
}
