package org.dse15.wait4op.klinisys.model.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by sylvia on 16.05.15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Patient extends User {

    private Gender _gender;
    private String _firstName;
    private String _lastName;
    private Location _location;

    @JsonProperty
    public Gender getGender() {
        return _gender;
    }

    public void setGender(Gender gender) {
        _gender = gender;
    }

    @JsonProperty
    public String getFirstName() {
        return _firstName;
    }

    public void setFirstName(String firstName) {
        _firstName = firstName;
    }

    @JsonProperty
    public String getLastName() {
        return _lastName;
    }

    public void setLastName(String lastName) {
        _lastName = lastName;
    }

    @Override
    public String toString() {
        return _lastName + " " + _firstName + " " + _gender +  ": " + super.toString();
    }

    @JsonProperty
    public Location getLocation() {
        return _location;
    }

    public void setLocation(Location location) {
        _location = location;
    }
}
