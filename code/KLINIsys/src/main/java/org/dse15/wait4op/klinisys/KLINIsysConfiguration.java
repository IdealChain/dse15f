package org.dse15.wait4op.klinisys;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * Created by sylvia on 16.05.15.
 */
public class KLINIsysConfiguration extends Configuration {

    private String _mongohost;
    private int _mongoport;
    private String _mongodb;

    @JsonProperty
    @NotEmpty
    public String getMongohost() {
        return _mongohost;
    }

    public void setMongohost(String mongohost) {
        _mongohost = mongohost;
    }

    @JsonProperty
    @Min(1)
    @Max(65535)
    public int getMongoport() {
        return _mongoport;
    }

    public void setMongoport(int mongoport) {
        _mongoport = mongoport;
    }

    @JsonProperty
    @NotEmpty
    public String getMongodb() {
        return _mongodb;
    }

    public void setMongodb(String mongodb) {
        _mongodb = mongodb;
    }
}
