package org.dse15.wait4op.klinisys.database;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.DBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.dse15.wait4op.klinisys.helper.Helper;
import org.dse15.wait4op.klinisys.model.slot.OPType;
import org.dse15.wait4op.klinisys.model.slot.Slot;
import org.dse15.wait4op.klinisys.model.user.Patient;
import org.dse15.wait4op.klinisys.model.user.User;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by sylvia on 17.05.15.
 */
public class DBFacade {

    private static DBFacade _facade;

    private DBFacade() {
    }

    public static DBFacade getInstance() {
        if(_facade == null)
            _facade = new DBFacade();

        return _facade;
    }

    public List<User> getAll() {
        FindIterable<Document> iterable = MongoConnection.getInstance().getUserCollection().find();

        List<User> rv = new LinkedList<>();

        return documentToObject(executeQuery(iterable));
    }

    public List<Slot> getAllSlots() {
        FindIterable<Document> iterable = MongoConnection.getInstance().getSlotCollection().find();

        List<Slot> rv = new LinkedList<>();

        return documentToSlot(executeQuery(iterable));
    }

    public String peristUser(User user) {
        MongoCollection<Document> collection = MongoConnection.getInstance().getUserCollection();

        ObjectMapper mapper = new ObjectMapper();

        String s = null;

        try {
            // map user object to JSON string
            s = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(user);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        Document d = Document.parse(s);

        collection.insertOne(d);
        return d.get("_id").toString();
    }

    public String persistSlot(Slot slot) throws Exception {
        MongoCollection<Document> collection = MongoConnection.getInstance().getSlotCollection();

        ObjectMapper mapper = new ObjectMapper();

        try {
            // map slot object to JSON string
            String s = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(slot);
            Document d = Document.parse(s);

            collection.insertOne(d);
            return d.get("_id").toString();

        } catch (JsonProcessingException e) {
            throw new Exception(e.getMessage(), e);
        }
    }

    public User getUserById(ObjectId id) {
        Document result = MongoConnection.getInstance().getUserCollection().find(
                new Document("_id", id)).first();

        return documentToUser(result);
    }

    public Slot getSlotById(ObjectId id) {
        FindIterable<Document> iterable = MongoConnection.getInstance().getSlotCollection().find(
                new Document("_id", id));

        List<Slot> slot = documentToSlot(executeQuery(iterable));

        if(slot.isEmpty())
            return null;

        return slot.get(0);
    }

    public User getUserByLogin(String loginName, String password) throws Exception {
        Document example = new Document();
        example.append("loginName", loginName == null ? "" : loginName);

        FindIterable<Document> iterable = MongoConnection.getInstance().getUserCollection().find(example);
        List<User> user = documentToObject(executeQuery(iterable));

        // check if a user with the given loginName and password exists
        for(User u : user) {
            if(Arrays.equals(Helper.createPassword(password), u.getPassword())) {
                return u;
            }
        }

        return null;
    }

    public List<User> getUserByType(String type) throws Exception {
        FindIterable<Document> iterable = MongoConnection.getInstance().getUserCollection().find(
                new Document("type", type));

        return documentToObject(executeQuery(iterable));
    }

    private List<User> documentToObject(List<Document> documents) {
        List<User> objects = new LinkedList<>();

        for(Document d : documents) {
            objects.add(documentToUser(d));
        }

        return objects;
    }

    private User documentToUser(Document document) {
        JsonFactory factory = new JsonFactory();
        ObjectMapper mapper = new ObjectMapper();
        User user = null;

            try {
                JsonParser parser = factory.createParser(document.toJson());

                user = mapper.readValue(parser, User.class);

                user.setId(document.getObjectId("_id").toString());

                return user;

            } catch (IOException e) {
                e.printStackTrace();
            }

        return user;
    }

    //TODO!!!
    private List<Slot> documentToSlot(List<Document> documents) {
        List<Slot> objects = new LinkedList<>();

        JsonFactory factory = new JsonFactory();
        ObjectMapper mapper = new ObjectMapper();

        for(Document d : documents) {

            try {
                Slot slot = new Slot();
                slot.setId(d.getObjectId("_id").toString());
                slot.setFrom(d.getLong("from"));
                slot.setTo(d.getLong("to"));
                slot.setHospitalId(d.getString("hospitalId"));
                slot.setType(OPType.parse(d.getString("type")));


                objects.add(slot);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return objects;
    }

    private List<Document> executeQuery(FindIterable<Document> iterable) {
        List<Document> rv = new LinkedList<>();

        iterable.forEach(new Block<Document>() {
            @Override
            public void apply(final Document document) {
                rv.add(document);
            }
        });

        return rv;
    }

    public void removeById(ObjectId id) {
        FindIterable<Document> iterable = MongoConnection.getInstance().getUserCollection().find(
                new Document("_id", id));

        List<Document> resultList = executeQuery(iterable);

        if(resultList.isEmpty())
            return;

        MongoConnection.getInstance().getUserCollection().deleteOne(resultList.get(0));

    }

    public void removeSlotById(ObjectId id) {
        FindIterable<Document> iterable = MongoConnection.getInstance().getSlotCollection().find(
                new Document("_id", id));

        List<Document> resultList = executeQuery(iterable);

        if(resultList.isEmpty())
            return;

        MongoConnection.getInstance().getSlotCollection().deleteOne(resultList.get(0));

    }

    public Document getLocation(ObjectId id) {
        FindIterable<Document> iterable = MongoConnection.getInstance().getUserCollection().find(
                new Document("_id", id));

        List<Document> resultList = executeQuery(iterable);

        Document d = (Document)resultList.get(0).get("location");

        return d;
    }

    public List<User> nearestHospitalToUser(ObjectId userId) throws IOException {

        Patient patient = (Patient) getUserById(userId);

        BasicDBObject query = new BasicDBObject("type", "HOSPITAL");

        DBObject cmd = new BasicDBObject();
        cmd.put("geoNear", "user");
        cmd.put("near", patient.getLocation().getCoordinates());
        cmd.put("spherical", true);
        cmd.put("query", query);

        Document result = MongoConnection.getInstance().getDatabase().runCommand((Bson) cmd);

        ArrayList<Document> a = (ArrayList<Document>) result.get("results");


        List<User> users = new LinkedList<>();

        for(Document d : a) {
            User u = documentToUser((Document)d.get("obj"));
            users.add(u);
        }

        return users;
    }
}
