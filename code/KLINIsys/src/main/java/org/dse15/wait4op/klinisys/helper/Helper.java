package org.dse15.wait4op.klinisys.helper;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.StringWriter;
import java.security.MessageDigest;

/**
 * Created by sylvia on 26.05.15.
 */
public class Helper {
    public static String objectToJson(Object o) throws IOException {
        ObjectMapper om = new ObjectMapper();
        StringWriter sw = new StringWriter();

        if (o == null)
            om.writeValue(sw, "{}");
        else
            om.writeValue(sw, o);

        return sw.toString();
    }

    public static byte[] createPassword(String plainPassword) throws Exception {
        if (plainPassword == null)
            plainPassword = "";
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        return digest.digest(plainPassword.getBytes("UTF-8"));
    }
}
