package org.dse15.wait4op.klinisys.model.user;

/**
 * Created by sylvia on 16.05.15.
 */
//@Entity
public enum Gender {
    FEMALE,
    MALE;
}
