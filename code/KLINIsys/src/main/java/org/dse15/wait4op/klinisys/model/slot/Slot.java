package org.dse15.wait4op.klinisys.model.slot;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Date;


/**
 * Created by sylvia on 26.05.15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Slot {

    private String _id;
    private Long _from;
    private Long _to;
    private OPType _type;
    private String _hospitalId;

    @JsonProperty("id")
    public String getId() {
        return _id;
    }

    public void setId(String id) {
        _id = id;
    }

    @JsonProperty("from")
    public Long getFrom() {
        return _from;
    }

    public void setFrom(Long from) {
        _from = from;
    }

    @JsonProperty("to")
    public Long getTo() {
        return _to;
    }

    public void setTo(Long to) {
        _to = to;
    }

    @JsonProperty("type")
    public OPType getType() {
        return _type;
    }

    public void setType(OPType type) {
        _type = type;
    }

    @JsonProperty("hospitalId")
    public String getHospitalId() {
        return _hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        _hospitalId = hospitalId;
    }

}
