package org.dse15.wait4op.klinisys;

import io.dropwizard.Application;
import io.dropwizard.setup.Environment;
import org.dse15.wait4op.klinisys.database.MongoConnection;
import org.dse15.wait4op.klinisys.resources.MasterData;
import org.dse15.wait4op.klinisys.resources.Slots;

/**
 *
 */
public class KLINIsysApplication extends Application<KLINIsysConfiguration> {

    public static void main(String args[]) throws Exception {
        new KLINIsysApplication().run(args);
    }

    @Override
    public void run(KLINIsysConfiguration config, Environment environment) throws Exception {
        MongoConnection.setupMongoConnection(config.getMongohost(), config.getMongoport(), config.getMongodb());

        environment.jersey().register(new MasterData());
        environment.jersey().register(new Slots());

    }
}
