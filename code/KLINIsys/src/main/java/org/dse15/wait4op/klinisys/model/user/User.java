package org.dse15.wait4op.klinisys.model.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Created by sylvia on 16.05.15.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class", defaultImpl = User.EmptyUser.class)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Doctor.class, name = "doctor"),
        @JsonSubTypes.Type(value = Patient.class, name = "patient"),
        @JsonSubTypes.Type(value = Hospital.class, name = "hospital")
})
public abstract class User {

    private String _id;
    private String _loginName;
    private Type _type;
    private byte[] _password;

    @JsonProperty("password")
    public byte[] getPassword() {
        return _password;
    }

    public void setPassword(byte[] password) {
        _password = password;
    }

    @JsonProperty("type")
    public Type getType() {
        return _type;
    }

    public void setType(Type type) {
        _type = type;
    }

    @JsonProperty("id")
    public String getId() {
        return _id;
    }

    public void setId(String id) {
        _id = id;
    }

    @JsonProperty("loginName")
    public String getLoginName() {
        return _loginName;
    }

    public void setLoginName(String loginName) {
        _loginName = loginName;
    }

    public String toString() {
        return "ID: " + _id +  "; LOGINNAME: " + getLoginName() + " Password: " + _password;
    }

    public class EmptyUser extends User {

    }
}
