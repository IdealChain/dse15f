package org.dse15.wait4op.klinisys.resources;

import org.bson.types.ObjectId;
import org.dse15.wait4op.klinisys.database.DBFacade;
import org.dse15.wait4op.klinisys.helper.Helper;
import org.dse15.wait4op.klinisys.model.user.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by sylvia on 16.05.15.
 */
@Path("/masterdata")
@Produces(MediaType.APPLICATION_JSON)
public class MasterData {

    @GET
    public Response getUser() throws Exception {

        DBFacade facade = DBFacade.getInstance();
        return Response.ok(Helper.objectToJson(facade.getAll())).build();
    }

    @GET()
    @Path("/login")
    public Response byLoginName(@QueryParam("username") String username, @QueryParam("password") String password) throws Exception {
        DBFacade facade = DBFacade.getInstance();
        String result;

        User user = facade.getUserByLogin(username, password);
        result = Helper.objectToJson(user);

        return Response.ok(result).build();
    }

    @GET
    @Path("/type")
    public Response byQuery(@QueryParam("type") String type) throws Exception {

        List<User> users = DBFacade.getInstance().getUserByType(type);
        String result = Helper.objectToJson(users);

        return Response.ok(result).build();
    }

    @GET
    @Path("/{userId}")
    public Response byId(@PathParam("userId") String userId) throws Exception {

        DBFacade facade = DBFacade.getInstance();
        User u = facade.getUserById(new ObjectId(userId));
        return Response.ok(Helper.objectToJson(u)).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response insertUser(UserTemplate user) throws Exception {

        DBFacade facade = DBFacade.getInstance();
        String id = null;
        switch (user.getType()) {
            case PATIENT:
                Patient p = new Patient();
                p.setFirstName(user.getFirstName());
                p.setLastName(user.getLastName());
                p.setGender(user.getGender());
                p.setType(Type.PATIENT);
                p.setLoginName(user.getLoginName());
                p.setPassword(Helper.createPassword(user.getPassword()));
                p.setLocation(user.getLocation());
                id = facade.peristUser(p);
                break;
            case DOCTOR:
                Doctor d = new Doctor();
                d.setFirstName(user.getFirstName());
                d.setLastName(user.getLastName());
                d.setTitle(user.getTitle());
                d.setType(Type.DOCTOR);
                d.setLoginName(user.getLoginName());
                d.setPassword(Helper.createPassword(user.getPassword()));
                id = facade.peristUser(d);
                break;
            case HOSPITAL:
                Hospital h = new Hospital();
                h.setType(Type.HOSPITAL);
                h.setLoginName(user.getLoginName());
                h.setName(user.getHospitalName());
                h.setPassword(Helper.createPassword(user.getPassword()));
                h.setLocation(user.getLocation());
                id = facade.peristUser(h);
                break;
        }
        return Response.ok(Helper.objectToJson(new Id(id))).build();
    }

    @DELETE
    @Path("/{userId}")
    public Response delete(@PathParam("userId") String userId) {

        DBFacade facade = DBFacade.getInstance();
        facade.removeById(new ObjectId(userId));
        return Response.ok().build();
    }

    @GET
    @Path("nearest/{userId}")
    public Response nearestHospital(@PathParam("userId") String userId) throws Exception {
        DBFacade facade = DBFacade.getInstance();

        List<User> users = facade.nearestHospitalToUser(new ObjectId(userId));

        return Response.ok(Helper.objectToJson(users)).build();
    }
}