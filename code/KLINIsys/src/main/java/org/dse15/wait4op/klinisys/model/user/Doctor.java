package org.dse15.wait4op.klinisys.model.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by sylvia on 16.05.15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Doctor extends User {

    private String _firstName;
    private String _lastName;
    private String _title;

    @JsonProperty
    public String getFirstName() {
        return _firstName;
    }

    public void setFirstName(String firstName) {
        _firstName = firstName;
    }

    @JsonProperty
    public String getLastName() {
        return _lastName;
    }

    public void setLastName(String lastName) {
        _lastName = lastName;
    }

    @JsonProperty
    public String getTitle() {
        return _title;
    }

    public void setTitle(String title) {
        _title = title;
    }

    @Override
    public String toString() {
        return (_title != null ? _title + " " : "") + _lastName + " " + _firstName + ": " + super.toString();
    }
}
