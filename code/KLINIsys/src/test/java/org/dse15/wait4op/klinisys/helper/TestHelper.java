package org.dse15.wait4op.klinisys.helper;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.dse15.wait4op.klinisys.model.slot.OPType;
import org.dse15.wait4op.klinisys.model.slot.Slot;
import org.dse15.wait4op.klinisys.model.user.Gender;
import org.dse15.wait4op.klinisys.model.user.Patient;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 */
public class TestHelper {

    /**
     * Checks if the password-hashing method works fine
     * @throws Exception
     */
    @Test
    public void testCreatePassword() throws Exception {
        Assert.assertEquals(256/8, Helper.createPassword("test").length);
    }

    /**
     * Checks if the general json-to-object and object-to-json case works fine
     * @throws Exception
     */
    @Test
    public void testObjectToJson() throws Exception {
        Slot s = new Slot();
        s.setType(OPType.HNO);
        String jsonString = Helper.objectToJson(s);

        JsonParser parser = new JsonFactory().createParser(jsonString);
        ObjectMapper om = new ObjectMapper();

        Slot result = om.readValue(parser, Slot.class);
        Assert.assertEquals(OPType.HNO, result.getType());
    }

    /**
     * Tests if the object-to-json and json-to-object inheritence case works fine
     * @throws Exception
     */
    @Test
    public void testObjectToJsonInheritance() throws Exception {
        Patient p = new Patient();
        p.setLoginName("login");
        p.setGender(Gender.FEMALE);
        String jsonString = Helper.objectToJson(p);

        JsonParser parser = new JsonFactory().createParser(jsonString);
        ObjectMapper om = new ObjectMapper();

        Patient result = om.readValue(parser, Patient.class);
        Assert.assertEquals("login", result.getLoginName());
        Assert.assertEquals(Gender.FEMALE, result.getGender());
    }
}
