#!/bin/bash -eu

masterdataPOST()
{
    curl -H "Content-Type: application/json" --silent -d "$1" localhost:8082/masterdata
    echo ""
}

slotPOST()
{
    curl -H "Content-Type: application/json" --silent -d "$1" localhost:8082/slots
    echo ""
}

echo "=================="
echo "Insert PATIENTs..."
echo "=================="
masterdataPOST '{"type": "PATIENT", "gender": "FEMALE", "loginName": "adelheid", "password": "adelheid", "firstName": "Adelheid", "lastName": "Abesser", "location": {"type": "Point", "coordinates": [16.3709743,48.2003216]}}'
masterdataPOST '{"type": "PATIENT", "gender": "MALE",   "loginName": "peter",    "password": "peter", "firstName": "Peter", "lastName": "Berger", "location": {"type": "Point", "coordinates": [16.373471,48.208411]}}'
masterdataPOST '{"type": "PATIENT", "gender": "FEMALE", "loginName": "beatrix",  "password": "beatrix", "firstName": "Beatrix", "lastName": "Bauer", "location": {"type": "Point", "coordinates": [16.420144,48.18503]}}'
masterdataPOST '{"type": "PATIENT", "gender": "MALE",   "loginName": "franz",    "password": "franz", "firstName": "Franz", "lastName": "Fiedler", "location": {"type": "Point", "coordinates": [16.395889,48.21662]}}'
masterdataPOST '{"type": "PATIENT", "gender": "FEMALE", "loginName": "gloria",   "password": "gloria", "firstName": "Gloria", "lastName": "Geraus", "location": {"type": "Point", "coordinates": [13.039894,47.799839]}}'
masterdataPOST '{"type": "PATIENT", "gender": "MALE",   "loginName": "manfred",  "password": "manfred", "firstName": "Manfred", "lastName": "Takacus", "location": {"type": "Point", "coordinates": [10.03153,47.37447]}}'

echo "=================="
echo "Insert DOCTORs... "
echo "=================="
masterdataPOST '{"type": "DOCTOR", "title": "Dr.", "loginName": "albert", "password": "albert", "firstName": "Albert", "lastName": "Aufschneider"}'
masterdataPOST '{"type": "DOCTOR", "title": "Dr.", "loginName": "emily",  "password": "emily", "firstName": "Emily", "lastName": "Ehmoser"}'
masterdataPOST '{"type": "DOCTOR", "title": "Dr.", "loginName": "adam",   "password": "adam", "firstName": "Adam", "lastName": "Augfehler"}'
masterdataPOST '{"type": "DOCTOR", "title": "Dr.", "loginName": "maria",  "password": "maria", "firstName": "Maria", "lastName": "Morks"}'

echo "=================="
echo "Insert CLINICs... "
echo "=================="
IDH1=$(masterdataPOST '{"type": "HOSPITAL", "loginName": "smzost",   "password": "smzost",   "hospitalName": "SMZ Ost", "location": {"type": "Point", "coordinates": [16.3193,48.23481]}}')
IDH2=$(masterdataPOST '{"type": "HOSPITAL", "loginName": "krems",    "password": "krems",    "hospitalName": "LKH Krems", "location": {"type": "Point", "coordinates": [15.60384,48.40999]}}')
IDH3=$(masterdataPOST '{"type": "HOSPITAL", "loginName": "baden",    "password": "baden",    "hospitalName": "LKH Baden", "location": {"type": "Point", "coordinates": [16.23091,48.00214]}}')
IDH4=$(masterdataPOST '{"type": "HOSPITAL", "loginName": "rudolf",   "password": "rudolf",   "hospitalName": "Rudolfinerhaus", "location": {"type": "Point", "coordinates": [16.347526,48.243109]}}')
IDH4=$(masterdataPOST '{"type": "HOSPITAL", "loginName": "salzburg", "password": "salzburg", "hospitalName": "LKH Salzburg", "location": {"type": "Point", "coordinates": [13.028781,47.805896]}}')
IDH4=$(masterdataPOST '{"type": "HOSPITAL", "loginName": "bregenz",  "password": "bregenz",  "hospitalName": "LKH Bregenz", "location": {"type": "Point", "coordinates": [9.745694,47.496954]}}')


echo "=================="
echo "Insert SLOTs...   "
echo "=================="
IDH1=$(echo $IDH1 | awk 'match($0, /\"[a-f0-9]*\"/) { print substr($0, RSTART, RLENGTH) }' | sed 's/\"//g')
IDH2=$(echo $IDH2 | awk 'match($0, /\"[a-f0-9]*\"/) { print substr($0, RSTART, RLENGTH) }' | sed 's/\"//g')
IDH3=$(echo $IDH3 | awk 'match($0, /\"[a-f0-9]*\"/) { print substr($0, RSTART, RLENGTH) }' | sed 's/\"//g')
IDH4=$(echo $IDH4 | awk 'match($0, /\"[a-f0-9]*\"/) { print substr($0, RSTART, RLENGTH) }' | sed 's/\"//g')

SLOT1=$(echo '{"hospitalId": "###", "from":"1440828000000", "to":"1440835200000", "type":"Augenheilkunde"}' | sed "s/###/${IDH1}/")
slotPOST "${SLOT1}"
SLOT2=$(echo '{"hospitalId": "###", "from":"1440828000000", "to":"1440835200000", "type":"HNO"}' | sed "s/###/${IDH1}/")
slotPOST "${SLOT2}"
SLOT3=$(echo '{"hospitalId": "###", "from":"1440828000000", "to":"1440835200000", "type":"Augenheilkunde"}' | sed "s/###/${IDH1}/")
slotPOST "${SLOT3}"
SLOT4=$(echo '{"hospitalId": "###", "from":"1440828000000", "to":"1440835200000", "type":"HNO"}' | sed "s/###/${IDH1}/")
slotPOST "${SLOT4}"
