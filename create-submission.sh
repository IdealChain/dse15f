#!/bin/bash -eu
LVA="DSE15"
GROUP="13"
SUBMISSION="${LVA}Gruppe${GROUP}Abgabe.zip"

echo Packing code
git archive --format zip --prefix=3_Code/ -v -o $SUBMISSION HEAD code README.md

echo Building documents
for m in $(find doc/ -name "Makefile" -printf "%h\n" | sort); do
    echo - $m
    make -C "$m" &> /dev/null
    zip -j $SUBMISSION "$m/${m##*/}.pdf"
done

echo All done.
