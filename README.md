dse15
=====

Distributed Systems Engineering VU  
Team 13

*SS2015, TU Wien*

Requirements
------------

### Development ###

* JDK 8
* Apache Maven
* Node.js (and npm)
* MongoDB daemon
* IntelliJ IDEA IDE

### Docker deployment ###

* [Docker](https://docs.docker.com/userguide/)
* [Docker Compose](https://docs.docker.com/compose/)
* ... and a few GB hard disk space for the created docker images

1. DEVELOPMENT
--------------

### 1.1 Setup dependencies ###

First, install the MongoDB database daemon and start it up.

Afterwards, fetch all maven/npm dependencies and run all component tests by issueing:

```
#!shell
$ cd code
$ mvn test
```

### 1.2 IntelliJ IDEA ###

Open the folder `code` as a project in IntelliJ. You should see all projects, and a bunch of run configurations.

Run the `Wait4OP Mainframe` configuration for starting up the whole system locally.

### 1.3 Testdata Insertion ###

If you want to have some test patients, doctors, clinics and slots to play with, start KLINIsys and
run the database setup script:

```
#!shell
$ cd code
$ ./KLINIsys/setup/setup-database.sh
```

2. DEPLOYMENT WITH DOCKER
-------------------------

### 2.1 Creating docker images ###

Issue the following command to create and start all containers making up the application.

This is going to take a while!

```
#!shell
$ cd code
$ docker-compose up

....

Attaching to code_klinisysDB_1, code_reservationDB_1, code_reservation_1, code_klinisys_1, code_notifier_1, code_opmatcher_1, code_gateway_1, code_storefront_1

...

storefront_1    | WebUI serving on http://0.0.0.0:8000. Come get yours!
storefront_1    | Secure WebUI serving on https://0.0.0.0:8443.
```

If everything worked out ok, you can view and use the web UI by pointing your browser to

* http://localhost or 
* https://localhost.

### 2.2 Testdata Insertion ###

If you want to have some test patients, doctors, clinics and slots to play with, run the following command to execute
the insertion script within the KLINIsys container:

```
#!shell
$ docker exec code_klinisys_1 setup/setup-database.sh
```

#### Attribution

Surgery Favicon by [freepik](http://www.flaticon.com/authors/freepik) licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)  
